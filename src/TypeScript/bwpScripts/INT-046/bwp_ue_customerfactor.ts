/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */


/**
 * User event script built to apply on customer record
 */

import type { EntryPoints } from 'N/types';
import * as log from 'N/log';
import * as uiMessageModule from 'N/ui/message';
import * as errorModule from 'N/error';
import * as searchModule from 'N/search';
import { Record } from '@hitc/netsuite-types/N/record';

export let beforeLoad: EntryPoints.UserEvent.beforeLoad = (scriptContext) => {
    log.audit({
        title: 'beforeLoad',
        details: 'Started',
    });
    if (scriptContext.request) {
        try {
            let customerRec = scriptContext.newRecord;
            if (isEligibleToFactor(customerRec, true)) {
                validateCustomer(customerRec);
            };
        } catch (error) {
            scriptContext.form.addPageInitMessage({
                type: uiMessageModule.Type.WARNING,
                message: error.message,
                title: 'Factor validation error',
            });
        }
    }
};

export let beforeSubmit: EntryPoints.UserEvent.beforeSubmit = (scriptContext) => {
    log.audit({
        title: 'beforeSubmit',
        details: 'Started',
    });
    if (scriptContext.type === scriptContext.UserEventType.CREATE 
        || scriptContext.type === scriptContext.UserEventType.EDIT
        || scriptContext.type === scriptContext.UserEventType.COPY){
        let customerRec = scriptContext.newRecord;

        if (isEligibleToFactor(customerRec, false)) {
            customerRec.setValue({ 
                fieldId: 'custentity_bwp_factor',
                value: true,
            });
        } else {
            customerRec.setValue({ 
                fieldId: 'custentity_bwp_factor',
                value: false,
            });
        }
    }
};

function validateCustomer(customerRec: Record) {
    let billAddress = retrieveBillingAddress(customerRec);
    let address1 = billAddress.getValue({ fieldId: 'addr1' });
    let city = billAddress.getValue({ fieldId: 'city' });
    let zip = billAddress.getValue({ fieldId: 'zip' });
    if (!address1 || !city || !zip) {
        let message = 'Fields address1, city and zip must be populated on billing address';
		log.audit({
			title: `customer ${customerRec.getValue({ fieldId: 'entityid' })}`,
			details: message, 
		});
        throw errorModule.create({
            name: 'BWP_INVALID_BILLING_ADDRESS',
            message: message,
            notifyOff: true,
        });
    }    
}

function isEligibleToFactor(customerRec: Record, throwError: boolean) {
    try {
        // perform checks in this order: Siren, Customer Type, Interco, Country
        // Check country last because it can raise an error, we don't want to display an error in case other checks are false
        return checkSiret(customerRec) && checkCustomerType(customerRec) && checkIntercoCode(customerRec) && checkCountry(customerRec) ;
    } catch (error) {
        if (throwError) {
            throw error;
        }
        return false;
    }
}

function checkSiret(customerRec: Record) {
    let siret = customerRec.getValue({ fieldId: 'custentity_bwp_siret' }) as string;
    return  (siret?.trim().length >= 9);
}

/**
 * Check if the country of the customer belongs to the list of eligible countries.
 * When country is not valid the corresponding input data must not be exported
 * @returns 
 */
function checkCountry(customerRec: Record) {
	// France, Guadeloupe, Guyane française, Réunion, Martinique, Mayotte
	const eligibleCountries = ['FR', 'GP', 'GF', 'RE', 'MQ', 'YT'];
    
    let billAddress = retrieveBillingAddress(customerRec);
    let country = billAddress.getValue({ fieldId: 'country' }) as string;
    if (!country) {
        // Should not occur, NetSuite does not allow to create an address without country
        let message = 'Empty country on billing address for this customer';
        log.audit({
            title: `customer ${customerRec.getValue({ fieldId: 'entityid' })}`,
            details: message,
        });
        throw errorModule.create({
            name: 'BWP_EMPTY_COUNTRY',
            message: message,
            notifyOff: true,
        });
    }

    return (eligibleCountries.includes(country));
}

function checkCustomerType(customerRec) {
    // Retrieve internal id for value 'Financeur' of custom list customlist_bwp_typologie_client
    let customerTypesSearchObj = searchModule.create({
        type: 'customlist_bwp_typologie_client',
        filters: [['name', 'is', 'Financeur']],
        columns: ['internalid']
    });
    const fincanceurInternalId = customerTypesSearchObj.run().getRange({ start: 0, end: 1 })[0]?.getValue({ name: 'internalid'});
    let customerType = customerRec.getValue({ fieldId: 'custentity_bwp_typologie_client' });
    return !(customerType == fincanceurInternalId);
}

function checkIntercoCode(customerRec) {
    // Retrieve internal id for value HGRP of custom segment cseg_bwp_codeinterc
    let intercoCodeSearchObj = searchModule.create({
        type: 'customrecord_cseg_bwp_codeinterc',
        filters: [['name', 'is', 'HGRP']],
        columns: ['internalid']
    });
    const intercoCodeHGRP = intercoCodeSearchObj.run().getRange({ start: 0, end: 1 })[0]?.getValue({ name: 'internalid' });
    let intercoCode = customerRec.getValue({ fieldId: 'cseg_bwp_codeinterc' }) as string;
    return intercoCode == intercoCodeHGRP;
}

function retrieveBillingAddress(customerRec) {
    let line = customerRec.findSublistLineWithValue({
		sublistId: 'addressbook',
		fieldId: 'defaultbilling',
		value: true
	});
	if (line >= 0) {
		return customerRec.getSublistSubrecord({ 
			sublistId: 'addressbook',
			fieldId: 'addressbookaddress',
			line: line
		});
	} else {
        let message = 'There is no billing address defined for this customer';
		log.audit({
			title: `customer ${customerRec.getValue({ fieldId: 'entityid' })}`,
			details: message, 
		});
        throw errorModule.create({
            name: 'BWP_NO_BILLING_ADDRESS',
            message: message,
            notifyOff: true,
        });
	}
}
