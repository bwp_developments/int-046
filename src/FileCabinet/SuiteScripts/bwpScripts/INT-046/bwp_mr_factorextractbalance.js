var ERRORMODULE, FILEMODULE, FORMATMODULE, QUERYMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE;

/**
 * This script is in charge to generate a temporary Customer_nnn file in Work forlder
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/error', 'N/file', 'N/format', 'N/query', 'N/record', 'N/runtime', 'N/search'], runMapReduce);

function runMapReduce(error, file, format, query, record, runtime, search) {
	ERRORMODULE= error;
	FILEMODULE= file;
	FORMATMODULE= format;
	QUERYMODULE= query;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	
	let returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	// returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
 * @param {Object} inputContext
 * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Object} inputContext.ObjectRef - Object that references the input data
 * @typedef {Object} ObjectRef
 * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
 * @property {string} ObjectRef.type - Type of the record instance that contains the input data
 * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
 * @since 2015.2
 */
function _getInputData(inputContext) {	
	log.debug('getInputData started');
	// dayDateFormatted();
	
	// Retrieve and validate scripts parameters
	const scriptParams = retrieveScriptParameters();
	if (!scriptParams.extractionId) {
		log.debug({
			title:'Process stopped',
			details: 'Extraction id is empty'
		});
		return;
	}
	
	log.debug({
		title:'Processing factor extration',
		details: scriptParams.extractionId
	});

	// Retrieve and validate data from factor extraction record 
	let factorRec = RECORDMODULE.load({
		type: 'customrecord_bwp_factorextraction',
		id: scriptParams.extractionId,
		isDynamic: false
	});
	const factorStep = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractstep' });
	if (factorStep != 2) {
		log.debug({
			title:'Process stopped',
			details: `Invalid factor step ${factorStep}`
		});
		return;
	}
	const subsidiary = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractsubsidiary' });
	log.debug({
		title:'Subsidiary',
		details: `${subsidiary} - ${factorRec.getText({ fieldId: 'custrecord_bwp_factorextractsubsidiary' })}`
	});

	let sqlString = generateExtractionSuiteQL({
		subsidiary: subsidiary,
		extractionId: scriptParams.extractionId
	});
	// // VP-102
	// sqlString += ' and t1.id in (19854, 44637) ';
	// sqlString += ' and t1.id in (2330, 2820, 28592, 8807) '; // CDCLAV001809, CDFV033026, CUSPAYMVSOFT0000004, VSOFT_REPRISE_GL
	// sqlString += ' and t1.id in (391689) ';
	// logVar(sqlString, 'sqlString');

	log.debug('getInputData done');
	return {
		type: 'suiteql',
		query: sqlString,
	};

	let inputdata = [];
	let pagedData = QUERYMODULE.runSuiteQLPaged({
		query: sqlString,
		// pageSize: 1000,
   	});
	logRecord(pagedData, 'pagedData');
	logVar(pagedData.count, 'pagedData.count');
	let resultIterator = pagedData.iterator();
	// let resultIterator = QUERYMODULE.runSuiteQLPaged({
	// 	query: sqlString,
	// 	pageSize: 2000,
   	// }).iterator();
	resultIterator.each(page => {
		let pageIterator = page.data.iterator();
		pageIterator.each(row => {
			logVar(row, 'row');
			let taggedValues = tagValues(row);
			logRecord(taggedValues, 'taggedValues');
		});
	});
	return {};


	// let suiteQLResults = QUERYMODULE.runSuiteQL({
	// 	query: sqlString
	// });
	// logRecord(suiteQLResults.results.length, 'suiteQLResults count');
	// logRecord(suiteQLResults.results, 'suiteQLResults');
	// return suiteQLResults.results;
}

/**
 * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
 * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
 * context.
 * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
 *     is provided automatically based on the results of the getInputData stage.
 * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
 *     function on the current key-value pair
 * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
 *     pair
 * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} mapContext.key - Key to be processed during the map stage
 * @param {string} mapContext.value - Value to be processed during the map stage
 * @since 2015.2
 */
function _map(context) {
	//log.debug('map started');

	// log.debug('map context', context.value);
	processMapValue(context);
	
	//log.debug('map done');
}

/**
 * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
 * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
 * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
 *     provided automatically based on the results of the map stage.
 * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
 *     reduce function on the current group
 * @param {number} reduceContext.executionNo - Number of times the reduce function has been executed on the current group
 * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} reduceContext.key - Key to be processed during the reduce stage
 * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
 *     for processing
 * @since 2015.2
 */
function _reduce(context) {
//	log.debug('reduce started');
	
	//log.debug('reduce done');
}

/**
 * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
 * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
 * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
 * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
 *     script
 * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
 * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
 * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
 * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
 *     script
 * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
 * @param {Object} summaryContext.inputSummary - Statistics about the input stage
 * @param {Object} summaryContext.mapSummary - Statistics about the map stage
 * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
 * @since 2015.2
 */
function _summarize(summary) {
	log.debug('summary started');
//	log.debug('summary', summary);

	const scriptParams = retrieveScriptParameters();
	if (!scriptParams.extractionId) return;

	if (writeErrorsToFlatFile(summary, scriptParams.extractionId) == 0) {
		let fileIds = writeDataToFlatFile(summary, scriptParams.extractionId);
		RECORDMODULE.submitFields({
			type: 'customrecord_bwp_factorextraction',
			id: scriptParams.extractionId,
			values: { 
				custrecord_bwp_factorextractstep: 3,
				custrecord_bwp_factorextractbalfileid: fileIds.balance,
				custrecord_bwp_factorextracttranfileid: fileIds.transaction
			}
		});
	} else {
		RECORDMODULE.submitFields({
			type: 'customrecord_bwp_factorextraction',
			id: scriptParams.extractionId,
			values: { 
				custrecord_bwp_factorextracterrormsg: 'Errors on Balance Extraction - see error file for details',
				custrecord_bwp_factorextractstep: 3
			}
		});
	}
	
	log.debug('summary done');
}

/**
 * 
 * @param {Object} paramObject 
 * @param {number} paramObject.subsidiary 
 * @param {number} paramObject.extractionId
 * @param {number} paramObject.transaction
 */
function generateExtractionSuiteQL(paramObject) {
	// The function must receive either transaction parameter or both subsidiary and extractionId
	if (!paramObject.transaction && !(paramObject.subsidiary && paramObject.extractionId)) {
		return;
	}

	let suiteQLQuery = `Select t11.custrecord_bwp_code_vendeur_cedant, 
			decode(upper(t1.type), 'JOURNAL', t2.entity, t1.entity) as entityref, t13.entityid, 
			t1.id transacid, t1.tranid, 
			t1.trandate, 
			t12.name as currencyname, 			
			Case When upper(t1.type) = 'CUSTCRED' and upper(t1.status) = 'B' or upper(t1.type) = 'CUSTINVC' and upper(t1.status) = 'B'
				Then t2.netamount 
				Else nvl(t2.foreignamountunpaid, 0) - nvl(t2.foreignpaymentamountunused, 0) 
			End as netamount, 
			nvl(t1.duedate, t1.trandate) as tranduedate, 
			t1.type, 
			t15.name as factortypename, 
			${paramObject.extractionId || 0} as factortoextract,
			-- Fields for control
			t1.custbody_bwp_payment_method, t14.custrecord_bwp_factor_type, 
			decode(t1.type, 'Journal', t2.custcol_bwp_exclude_factor, t1.custbody_bwp_exclude_factor) as excludefactor, 
			t1.status, t2.subsidiary, t2.createdfrom, t11.name, t1.entity, 
			decode(t1.type, 'Journal', t2.cseg_bwp_codeinterc, t1.cseg_bwp_codeinterc) codeinterco
		from transaction t1
			inner join transactionline t2
			  on t2.transaction = t1.id
			inner join account t3
			  on t3.id = t2.expenseaccount
			inner join customersubsidiaryrelationship t4
			  on t4.subsidiary = t2.subsidiary
			  and t4.entity = decode(t1.type, 'Journal', t2.entity, t1.entity)
			left outer join customrecord_bwp_transactionexports t5
			  on t5.custrecord_bwp_transactionexportstransac = t1.id
			inner join subsidiary t11
			  on t11.id = t2.subsidiary
			inner join currency t12
			  on t12.id = t11.currency
			inner join customer t13
			  on t13.id = t4.entity
			left outer join customrecord_bwp_payment_method_list t14
			  on t14.id = t1.custbody_bwp_payment_method
			left outer join customlist_bwp_factor_type t15
			  on t15.id = t14.custrecord_bwp_factor_type
		where t3.accttype = 'AcctRec' 
		  and t12.name = 'EUR' `;

	let additionnalFilters = '';
	if (paramObject.transaction) {
		additionnalFilters += ` and t1.id in (${paramObject.transaction}) `;
	}
	if (paramObject.subsidiary) {
		// Retrive data from subsidiary record
		let fieldsValues = SEARCHMODULE.lookupFields({
			type: SEARCHMODULE.Type.SUBSIDIARY,
			id: paramObject.subsidiary,
			columns: ['custrecord_bwp_factorextractdate']
		});
		let lastExtractionDate;
		if ('custrecord_bwp_factorextractdate' in fieldsValues && fieldsValues.custrecord_bwp_factorextractdate) {
			let textDate = fieldsValues.custrecord_bwp_factorextractdate;
			lastExtractionDate = FORMATMODULE.parse({
				value: textDate,
				type: FORMATMODULE.Type.DATE
			});
		};

		// Retrieve internal id for value HGRP of custom segment cseg_bwp_codeinterc
		let intercoCodeSearchObj = SEARCHMODULE.create({
			type: 'customrecord_cseg_bwp_codeinterc',
			filters: [['name', 'is', 'HGRP']],
			columns: ['internalid']
		});
		const intercoCodeHGRP = intercoCodeSearchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid' });

		additionnalFilters += ` and t13.custentity_bwp_factor = 'T' 
		    --and nvl(decode(t1.type, 'Journal', t2.custcol_bwp_exclude_factor, t1.custbody_bwp_exclude_factor), 'F') = 'F'
			and nvl(t1.custbody_bwp_exclude_factor, 'F') <> 'T' and nvl(t2.custcol_bwp_exclude_factor, 'F') <>'T'
			and decode(t1.type, 'Journal', t2.cseg_bwp_codeinterc, t1.cseg_bwp_codeinterc) = ${intercoCodeHGRP} 
			and (t1.type = 'CustPymt' and t1.foreignpaymentamountunused <> 0 
				or t1.type = 'CustCred' and t1.status in ('A', 'CustCred:A') 
				or t1.type = 'CustInvc' and t1.status in ('A', 'CustInvc:A') 
				or t1.type = 'Journal' and t1.status in ('B', 'Journal:B') 
				or t1.type = 'CustCred' and t1.status in ('B', 'CustCred:B') and nvl(t5.custrecord_bwp_invoice_factored, 'F') = 'F' 
				and ${generateFilterOnTranDate(lastExtractionDate)} 
			) 
			and t1.reversal is null 
			and t2.subsidiary = ${paramObject.subsidiary} `;
	}

	suiteQLQuery += additionnalFilters;
	logVar(suiteQLQuery, 'suiteQLQuery');
	return suiteQLQuery;
}

function tagValues(values) {
	// logRecord(values, 'values');
	const tags = ['custrecord_bwp_code_vendeur_cedant', 'entity', 'entityid', 'internalid', 'tranid', 'trandate', 'currencyName',
		'netamount', 'duedate', 'type', 'custrecord_bwp_factor_typeName', 'factortoextract', 'custbody_bwp_payment_method', 
		'custrecord_bwp_factor_type', 'excludefactor', 'status', 'subsidiary', 'createdfrom'];
	let taggedValues = {};
	for (let i = 0 ; i < tags.length ; i++) {
		taggedValues[tags[i]] = values[i];
	}
	logRecord(taggedValues, 'taggedValues');
	return taggedValues;
}

function processMapValue(mapContext, values) {
	if (!values) {
		values = JSON.parse(mapContext.value).values;
	}
	// With a SuiteQL query the result is an array ==> it is tagged with the columns names to make it easier to use	
	let taggedValues = tagValues(values);
	// logRecord(taggedValues, 'taggedValues');
	 
	let transactionInternalId = taggedValues['internalid'];
	let createdFromProcessResult;
	if (taggedValues['type'].toUpperCase() == 'CUSTCRED' && taggedValues['status'].toUpperCase() == 'B') {
		let createdFrom = taggedValues['createdfrom'];
		if (createdFrom) {
			if (!checkOriginalInvoice(createdFrom, taggedValues['netamount'])) {
				// Do not process a credit memo fully apllied if the related invoice does not match some criteria
				log.debug({
					title: 'Credit memo not processed',
					details: `Credit memo ${taggedValues['tranid']} is Fully Applied and not extracted because created from document is not flagged as exported or does not have the same amount`
				});
				return;
			} else 
			{
				logVar(transactionInternalId, 'Created from is extracted')
				let createdFromResults = QUERYMODULE.runSuiteQL({
					query: generateExtractionSuiteQL({
							transaction: createdFrom //120694
						}),
				});
				// logRecord(createdFromResults, 'createdFromResults');
				createdFromProcessResult = processMapValue(mapContext, createdFromResults.results[0].values);
				log.debug({
					title: 'Createdfrom extraction',
					details: `Document ${createdFromProcessResult.tranid} associated to ${taggedValues['tranid']} is extracted`
				});
			}
		}
	}
	let itemRecord = '';
	if (['CUSTCRED', 'CUSTINVC'].includes(taggedValues['type'].toUpperCase())) {
		itemRecord = getTransactionItemRecord(transactionInternalId);
	}

	let writeContext = generateOutputLine({
		sqlValues: taggedValues,
		itemRecord: itemRecord
	});

	// In case of created from record processMapValue() function is called as a subfonction, be sure both original and created from transactions are exported
	if (createdFromProcessResult !== undefined) {
		if (createdFromProcessResult.exported && !writeContext.value) {
			throw ERRORMODULE.create({
				name: 'BWP_INCOMPLETE_EXTRACT',
				message: `Document ${createdFromProcessResult.tranid} is extracted but ${taggedValues['tranid']} is not extracted`
			});
		}
		if (!createdFromProcessResult.exported && writeContext.value) {
			throw ERRORMODULE.create({
				name: 'BWP_INCOMPLETE_EXPORT',
				message: `Document ${taggedValues['tranid']} is exported but ${createdFromProcessResult.tranid} is not extracted`
			});
		}
	}
	if (writeContext.value) {
		mapContext.write(writeContext);
		mapContext.write({
			key: 'TransactionProcessed',
			value: [transactionInternalId, taggedValues['type'], taggedValues['tranid']].join(retrieveScriptParameters().fieldSeparator)
		});
		return {
			tranid: taggedValues['tranid'],
			exported: true
		};
	}
	return {
		tranid: taggedValues['tranid'],
		exported: false
	};
}

/**
 * Check if a fully applied cust cred must be sent to Factor.
 * Such a cust cred must be sent to factor if the original invoice satisfies the following conditions:
 * - the invoice was sent to Factor
 * - the total amount of the invoice matches the credit amount
 * @param {*} transaction 
 * @param {*} creditAmount 
 * @returns 
 */
function checkOriginalInvoice(transaction, creditAmount){
	let toBeExtracted;
	if (transaction) {
		let invoiceInfo = retrieveOriginalInvoiceInfo(transaction);
		toBeExtracted = invoiceInfo.invoiceFactored && ((parseFloat(creditAmount) * (-1)) == parseFloat(invoiceInfo.amount));
		log.debug({
			title:'Check Original Invoice', 
			details: `CreatedFrom: ${transaction} ; Factored: ${invoiceInfo.invoiceFactored} ; Amount: ${invoiceInfo.amount} ; CreditAmount: ${creditAmount}`
		});
	}
	return toBeExtracted;
}

function retrieveOriginalInvoiceInfo(transaction) {
	let originalTransactionInfo = retrieveOriginalTransactionInfo(transaction);
	let transactionExportsInfo = retrieveTransactionExportsInfo(transaction);

	return {
		amount: originalTransactionInfo.amount,
		invoiceFactored: transactionExportsInfo.invoiceFactored
	};
}

function retrieveOriginalTransactionInfo(transaction) {
	let amount = 0;
	let fieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.INVOICE,
		id: transaction,
		columns: ['amount']
	});
	if ('amount' in fieldsValues) {
		amount = fieldsValues['amount'];
	}
	return {
		amount: amount
	};
}

function retrieveTransactionExportsInfo(transaction) {
	let invoiceFactored = false;
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_bwp_transactionexports',
		filters: [['custrecord_bwp_transactionexportstransac', 'anyof', transaction]] ,
		columns: ['internalid', 'custrecord_bwp_invoice_factored']
	});
	let results = searchObj.run().getRange({ start: 0, end: 1 });
	if (results.length > 0) {
		invoiceFactored = results[0].getValue({ name: 'custrecord_bwp_invoice_factored'});
	}

	return {
		invoiceFactored: invoiceFactored
	};
}

/**
 * 
 * @param {Object} dateObj
 * @returns 
 */
function generateFilterOnTranDate(dateObj) {
	// let dateFormatted = `${regexResult[3]}/${regexResult[2]}/${regexResult[1]}`;
	// logRecord(dateFormatted, 'dateFormatted');
	if (dateObj) {
		const regex = /^(\d+)-(\d+)-(\d+)/;
		let regexResult = regex.exec(dateObj.toISOString());
		return `t1.trandate >= to_date('${regexResult[1]}/${regexResult[2]}/${regexResult[3]}', 'yyyy/mm/dd')`
	} else {
		return '1 = 1'
	}
}

function getTransactionItemRecord(transactionId) {
	let itemRecord;
	let transacSearch = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.TRANSACTION,
		filters: ['internalid', 'anyof', transactionId],
		columns: ['internalid', 'tranid']
	});
	// let result = transacSearch.run().getRange({ start: 0, end: 1 })[0];
	// logRecord(result, 'result');
	// return itemRecord;
	let transactionType = transacSearch.run().getRange({ start: 0, end: 1 })[0].recordType;
	let transacRecord = RECORDMODULE.load({
		type: transactionType,
		id: transactionId,
		isDynamic: false
	});
	let lineCount = transacRecord.getLineCount({ sublistId: 'item' });
	let item;
	for (let i = 0 ; i < lineCount ; i++) {
		let itemField = transacRecord.getSublistValue({
			sublistId: 'item',
			fieldId: 'item',
			line: i
		});
		if (parseInt(itemField, 10) > 0) {
			item = itemField;
			break;
		}
		// if (item) {
		// 	break;
		// }
	}
	if (item) {
		let itemSearch = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.ITEM,
			filters: ['internalid', 'anyof', item],
			columns: ['internalid', 'itemid']
		});
		let itemType;
		// Eric Moulin - 01/03/2022 - it appears that the item search can fail 
		// --> catch the failure and report an audit line in the log
		try {
			itemType = itemSearch.run().getRange({ start: 0, end: 1 })[0].recordType;
		} catch (ex) {
			let tranid = transacRecord.getValue({ fieldId: 'tranid' });			
			log.audit({
				title: 'Invalid item',
				details: `Failed to retrieve item ${item} referenced by transaction ${tranid} (${transactionId})`
			});
		}
		if (itemType) {
			itemRecord = RECORDMODULE.load({
				type: itemType,
				id: item,
				isDynamic: false
			});
		}
		// log.debug({
		// 	title: 'item text',
		// 	details: itemRecord.getText({ fieldId: 'item' })
		// });
		// log.debug({
		// 	title: 'cseg_bwp_metier value',
		// 	details: itemRecord.getValue({ fieldId: 'cseg_bwp_metier' })
		// });
		// log.debug({
		// 	title: 'cseg_bwp_metier text',
		// 	details: itemRecord.getText({ fieldId: 'cseg_bwp_metier' })
		// });
		// log.debug({
		// 	title: 'custitem_bwp_activity value',
		// 	details: itemRecord.getValue({ fieldId: 'custitem_bwp_activity' })
		// });
		// log.debug({
		// 	title: 'custitem_bwp_activity text',
		// 	details: itemRecord.getText({ fieldId: 'custitem_bwp_activity' })
		// });
	}
	return itemRecord;
}

function writeErrorsToFlatFile(summary, extractionId) {
	let errorCount = 0;
	const fileName = `Balance_Errors_${extractionId}`;
	const folder = errorFolderPath();
	const folderId = retrieveFolderId(folder);
	let fileObj;
	summary.mapSummary.errors.iterator().each(
		function(key, error, executionNo) {
			if (!fileObj) {
				fileObj = FILEMODULE.create({
					name: fileName,
					fileType: FILEMODULE.Type.PLAINTEXT,
					description: 'Balance Factor Extract Errors',
					encoding: FILEMODULE.Encoding.UTF8,
					folder: folderId
				});
			}
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj.appendLine({
				value: message
			});
			errorCount++;
			// log.debug({
			// 	title: 'error',
			// 	details: error
			// });
			return true;
		}
	);
	if (fileObj) {
		fileObj.save();
		log.debug({
			title: 'File created',
			details: `${folder}/${fileName}`
		});
	}
	return errorCount;
}

function writeDataToFlatFile(summary, extractionId) {
	let fileIds = {
		balance: 0,
		transaction: 0
	};
	let factorRec = RECORDMODULE.load({
		type: 'customrecord_bwp_factorextraction',
		id: extractionId,
		isDynamic: false
	});
	const timestamp = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextracttimestamp' });

	// subsidiary's information
	let subsidiaryRec = RECORDMODULE.load({
		type: RECORDMODULE.Type.SUBSIDIARY,
		id: factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractsubsidiary' }),
		isDynamic: false
	});
	const codeVendeurCedant = subsidiaryRec.getValue({ fieldId: 'custrecord_bwp_code_vendeur_cedant' });

	// Generate first and last lines
	let firstLine = 'Not implemented';
	let lastLine = 'Not implemented';
	try {
		firstLine = generateFirstLastOutputLine(subsidiaryRec, true);
		lastLine = generateFirstLastOutputLine(subsidiaryRec, false);
	} catch (ex) {
		log.debug({
			title: 'error',
			details: ex
		});
	}

	// Set temporary filename and write data
	// const fileName = `${subsidiaryRec.getValue({ fieldId: 'custrecord_bwp_code_vendeur_cedant' })} - Export_Clients du ${nowDateDDMMYYYY()}`;
	// const fileName = `Balance_${extractionId}`;
	const fileName = `${codeVendeurCedant} - Export_Balance du ${timestamp}`;
	const folder = workFolderPath();
	const folderId = retrieveFolderId(folder);
	let fileObj;

	const transactionsFileName = `Transactions_${extractionId}`;
	let transactionsFileObj

	summary.output.iterator().each((key, value) => {
		switch (key) {
			case 'OutputLine':
				// log.debug({
				// 	title: key,
				// 	details: value
				// });
				if (!fileObj) {
					try {
						let oldFile = FILEMODULE.load({
							id: `${folder}/${fileName}`
						});
						FILEMODULE.delete({ id: oldFile.id });
					} catch (ex) { }
					fileObj = FILEMODULE.create({
						name: fileName,
						fileType: FILEMODULE.Type.PLAINTEXT,
						description: 'Balance Factor Extract ' + extractionId,
						encoding: FILEMODULE.Encoding.WINDOWS_1252,  // UTF8 does not work
						folder: folderId
					});
					fileObj.appendLine({
						value: `${firstLine}\r`
					});
				}
				fileObj.appendLine({
					value: `${value}\r`
				});
				break;
			case 'TransactionProcessed':
				// log.debug({
				// 	title: key,
				// 	details: value
				// });
				if (!transactionsFileObj) {
					transactionsFileObj = FILEMODULE.create({
						name: transactionsFileName,
						fileType: FILEMODULE.Type.PLAINTEXT,
						description: 'Transactions processed',
						encoding: FILEMODULE.Encoding.WINDOWS_1252,  // UTF8 does not work
						folder: folderId
					});
				}
				transactionsFileObj.appendLine({
					value: value
				});
				break;
		}
		return true;
	});
	if (fileObj) {
		fileObj.appendLine({
			value: `${lastLine}\r`
		});
		fileIds.balance = fileObj.save();
		log.debug({
			title: 'File created',
			details: `${folder}/${fileName}`
		});
	}
	if (transactionsFileObj) {
		fileIds.transaction = transactionsFileObj.save();
		log.debug({
			title: 'File created',
			details: `${folder}/${transactionsFileName}`
		});
	}
	return fileIds;
}

function retrieveScriptParameters() {	
	let script = RUNTIMEMODULE.getCurrentScript();
	return {
		extractionId: script.getParameter({ name: 'custscript_bwp_mr_factorextractidtran' }) || 0,
		fieldSeparator: script.getParameter({ name: 'custscript_bwp_mr_factorextractfieldseparator' }) || ';'
	}
}

function getCustomerAddress(customer) {
	let recordObj = RECORDMODULE.load({
		type: RECORDMODULE.Type.CUSTOMER,
		id: customer,
		isDynamic: false
	});
	let line = recordObj.findSublistLineWithValue({
		sublistId: 'addressbook',
		fieldId: 'defaultbilling',
		value: true
	});
	return recordObj.getSublistSubrecord({ 
		sublistId: 'addressbook',
		fieldId: 'addressbookaddress',
		line: line
	});
}

/**
 * 
 * @param {Object} resultValues 
 * @param {Object} resultValues.sqlValues 
 * @param {Record} resultValues.itemRecord
 * @returns 
 */
function generateOutputLine(resultValues) {
	return { 
		key: 'OutputLine',
		value: formatOutputLine(mapData(resultValues))
	};
}

function generateFirstLastOutputLine(subsidiaryRec, first) {
	return formatOutputLine(mapFisrtOrLastLine(subsidiaryRec, first));
}

/**
 * Map each value expected in the output with the corresponding value from the input
 * Set data processors that are in charge to validate and format the data to output
 * Data processors must be functions that return arrow function
 * @param {*} resultValues 
 * @returns {Object} outputData 
 * @returns {string} outputData.vendorCode
 * @returns {string} outputData.customerCode
 * @returns {string} outputData.customerIdentifier
 * @returns {string} outputData.customerAcronym
 * @returns {string} outputData.customerCompanyName
 * @returns {string} outputData.address
 * @returns {string} outputData.additionalAddress
 * @returns {string} outputData.postalCode
 * @returns {string} outputData.city
 * @returns {string} outputData.country
 * @returns {string} outputData.phone
 */
function mapData(resultValues) {
	// logRecord(resultValues, 'resultValues');
	let dataMap = {
		metadata: {
			sources: {
				transaction: `${resultValues.sqlValues['tranid']} (${resultValues.sqlValues['internalid']})`,
				customer: `${resultValues.sqlValues['entityid']} (${resultValues.sqlValues['entity']})`
			}
		},
		fields: {
			vendorCode: {
				fieldSource: 'subsidiary.custrecord_bwp_code_vendeur_cedant',
				sourceValue: resultValues.sqlValues['custrecord_bwp_code_vendeur_cedant'],
				dataProcessors: [checkMandatory(), truncate(6)]
			},
			extractionDate: {
				fieldSource: '',
				sourceValue: new Date(),
				dataProcessors: [truncate(10), formatDate()]
			},
			customerCode: {
				fieldSource: 'customer.entityid',
				sourceValue: resultValues.sqlValues['entityid'],
				dataProcessors: [truncate(10)]
			},
			documentNumber: {
				fieldSource: 'transaction.tranid',
				sourceValue: resultValues.sqlValues['tranid'],
				dataProcessors: [truncate(15)]
			},
			// documentNumber: {
			// 	fieldSource: 'transaction.internalid',
			// 	sourceValue: resultValues.sqlValues['internalid'],
			// 	dataProcessors: [truncate(15)]
			// },
			documentDate: {
				fieldSource: 'transaction.trandate',
				sourceValue: resultValues.sqlValues['trandate'],
				dataProcessors: [truncate(10), formatDate()]
			},
			currency: {
				fieldSource: 'transaction.currency.name',
				sourceValue: resultValues.sqlValues['currencyName'],
				dataProcessors: [truncate(3)]
			},
			domesticAmount: {
				fieldSource: 'transaction.netamount',
				sourceValue: resultValues.sqlValues['netamount'],
				dataProcessors: [truncate(15), formatAmount()]
			},
			dueDate: {
				fieldSource: 'transaction.duedate',
				sourceValue: resultValues.sqlValues['duedate'],
				dataProcessors: [truncate(10), formatDate()]
			},
			documentType: {
				fieldSource: 'transaction.type',
				sourceValue: resultValues.sqlValues['type'],
				dataProcessors: [truncate(3), transcoType()]
			},
			paymentMethod: {
				fieldSource: 'transaction.custbody_bwp_payment_method.custrecord_bwp_factor_type.name',
				sourceValue: resultValues.sqlValues['custrecord_bwp_factor_typeName'],
				dataProcessors: [truncate(3)]
			},
			commandNumber: {
				fieldSource: 'item.cseg_bwp_metier && item.custitem_bwp_activity',
				sourceValue: {
					cseg_bwp_metier: resultValues.itemRecord ? resultValues.itemRecord.getText({ fieldId: 'cseg_bwp_metier' }) : '',
					custitem_bwp_activity: resultValues.itemRecord ? resultValues.itemRecord.getText({ fieldId: 'custitem_bwp_activity' }) : ''
				},
				dataProcessors: [truncate(10), transcoFactorSalesType()]
			},
		}
	};
	// logRecord(dataMap, 'dataMap');
	return dataMap;
}

/**
 * The output file contains a header line and a footer line. They contain hardcoded values and information coming from the concerned subsidiary
 * @param {*} first 
 * @returns {Object} see mapData
 */
function mapFisrtOrLastLine(subsidiaryRec, first) {
	let dataMap = {
		metadata: {
			sources: {
				Hardcoded: first ? 'Fisrt line' : 'Last line',
			}
		},
		fields: {
			vendorCode: {
				fieldSource: 'Hardcoded.vendorCode',
				sourceValue: first ? '000000' : '999999',
				dataProcessors: [checkMandatory(), truncate(6)]
			},
			extractionDate: {
				fieldSource: 'Hardcoded.extractionDate',
				sourceValue: new Date(),
				dataProcessors: [truncate(10), formatDate()]
			},
			customerCode: {
				fieldSource: 'Hardcoded.customerCode',
				sourceValue: '',
				dataProcessors: [truncate(10)]
			},
			documentNumber: {
				fieldSource: 'Hardcoded.documentNumber',
				sourceValue: '',
				dataProcessors: [truncate(15)]
			},
			documentDate: {
				fieldSource: 'Hardcoded.documentDate',
				sourceValue: new Date(),
				dataProcessors: [truncate(10), formatDate()]
			},
			currency: {
				fieldSource: 'subsidiary.currency(Text)',
				sourceValue: subsidiaryRec.getText({ fieldId: 'currency' }),
				dataProcessors: [truncate(3)]
			},
			domesticAmount: {
				fieldSource: 'Hardcoded.domesticAmount',
				sourceValue: 0,
				dataProcessors: [truncate(15)]
			},
			dueDate: {
				fieldSource: 'Hardcoded.dueDate',
				sourceValue: 0,
				dataProcessors: [truncate(10)]
			},
			documentType: {
				fieldSource: 'Hardcoded.documentType',
				sourceValue: first ? 'DEB' : 'FIN',
				dataProcessors: [truncate(3)]
			},
			paymentMethod: {
				fieldSource: 'Hardcoded.paymentMethod',
				sourceValue: '',
				dataProcessors: [truncate(3)]
			},
			commandNumber: {
				fieldSource: 'Hardcoded.commandNumber',
				sourceValue: '',
				dataProcessors: [truncate(10)]
			},
		}
	};
	// logRecord(dataMap, 'outputData');
	return dataMap;
}

/**
 * Iterate over fields to ouput. Complete the list of data processors and apply them.
 * Handle formatting / validation exceptions (errors or data to exclude)
 * @param {*} outputData 
 * @returns 
 */
function formatOutputLine(outputData) {
	const scriptParams = retrieveScriptParameters();
	// Set a shorter constant for readibility
	const sep = scriptParams.fieldSeparator;
	applyFunctions(outputData);
	// logRecord(outputData, 'outputData');
	let outputValues = [];
	let fields = outputData.fields;
	// // for testing purpose
	// fields.vendorCode.fieldValue = '';
	for (let key in fields) {
		outputValues.push(replaceChars(sep, ' ')(fields[key].destValue));
	}
	return outputValues.join(sep);
}

function applyFunctions(dataMap) {
	// log.debug({
	// 	title: 'Function start',
	// 	details: 'applyFunctions'
	// });
	// const scriptParams = retrieveScriptParameters();
	// // Set a shorter constant for readibility
	// const sep = scriptParams.fieldSeparator;
	
	let fields = dataMap.fields;
	for (let key in fields) {
		try {
			fields[key].destValue = applyFieldFunctions(fields[key].sourceValue, [...(fields[key].dataProcessors || [])]);
		} catch (ex) {
			if (ex.name && ex.name.startsWith('BWP_') && ex.message) {
				const regex = /(^\w+)\.(.+)/;
				let regexRes = regex.exec(fields[key].fieldSource);
				let source = '';
				let field = '';
				let sourceInfo = '';
				if (regexRes) {
					source = regexRes[1];
					field = regexRes[2];
					sourceInfo = dataMap.metadata.sources[source];
				}
				let message = `${source} ${sourceInfo} - field ${field} - ${ex.message}`;
				if (ex.name == 'BWP_EXCLUDE_THIS_RESULT') {
					// This result may not be extracted ==> reset fields
					dataMap.fields = {};
					log.debug({
						title: 'Data exclusion',
						details: message
					});
					break;
				}
				throw ERRORMODULE.create({
					name: ex.name,
					message: message,
					notifyOff: true
				});
			}
			throw ex;
		}
	}
}

/**
 * apply data processors functions in a recursive way
 * @param {*} stringValue 
 * @param {*} functions 
 * @returns 
 */
function applyFieldFunctions(fieldValue, functions) {
	if (functions.length == 0) {
		return fieldValue;
	}
	let func = functions.pop();
	if (!(typeof func === 'function')) {
		func = a => a;
		log.debug('not a function');
	}
	return applyFieldFunctions(func(fieldValue), functions);
}

function transcoType() {
	const transcoTypes = {
		CUSTCRED: 'AVO',
		CUSTINVC: 'FAC',
		CUSTPYMT: 'RGT',
		JOURNAL: 'OD'
	};
	return b => transcoTypes[b.toUpperCase()];
}

/**
 * b is an object that contains
 * 	b.cseg_bwp_metier
 * 	b.custitem_bwp_activity
 * @returns 
 */
function transcoFactorSalesType() {
	// TODO: cache the transco
	// const RenewE = retrieveFactorSalesTypeId('RenewE');
	// const Renewl = retrieveFactorSalesTypeId('Renewl');
	// const Licences = retrieveFactorSalesTypeId('Licences');
	const transcoFactorSalesTypes = {
		'RECURRENT/EDITION': 'RenewE',
		'RECURRENT/*': 'Renewl',
		'*/*': 'Licences'
	};
	return b => (transcoFactorSalesTypes[`${b['custitem_bwp_activity'] || 'NA'}/${b['cseg_bwp_metier'] || 'NA'}`] || 
		transcoFactorSalesTypes[`${b['custitem_bwp_activity'] || 'NA'}/*`] || 
		transcoFactorSalesTypes[`*/*`]); // + `${b[0] || 'NA'}/${b[1] || 'NA'}`;
}

// function retrieveFactorSalesTypeId(factorText) {	
// 	// Retrieve internal id for a given value of custom list customlist_bwp_factor_sales_type_list
// 	let intercoCodeSearchObj = SEARCHMODULE.create({
// 		type: 'customlist_bwp_factor_sales_type_list',
// 		filters: [['name', 'is', factorText]],
// 		columns: ['internalid']
// 	});
// 	return intercoCodeSearchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid'});
// }

/**
 * 
 * @returns today's date in DD/MM/YYYY format
 */
function dayDateFormatted() {
	const regex = /^(\d+)-(\d+)-(\d+)/;
	let regexResult = regex.exec(new Date().toISOString());
	return `${regexResult[3]}/${regexResult[2]}/${regexResult[1]}`;
}

/**
 * 
 * @returns arrow function that format a date as a DD/MM/YYYY string
 */
function formatDate() {
	return b => {
		let dateToFormat;
		if (b instanceof Date) {
			dateToFormat = b;
		} else if (typeof b === 'string') {
			dateToFormat = FORMATMODULE.parse({
				value: b,
				type: FORMATMODULE.Type.DATE
			});
		}
		const regex = /^(\d+)-(\d+)-(\d+)/;
		let regexResult = regex.exec(dateToFormat.toISOString());
		// let dateFormatted = `${regexResult[3]}/${regexResult[2]}/${regexResult[1]}`;
		// logRecord(dateFormatted, 'dateFormatted');
		return `${regexResult[3]}/${regexResult[2]}/${regexResult[1]}`;
	}
}

function formatAmount() {
	return b => b && b.toString().length > 0 ? b.toString().replace(/\./g, ',') : b;
}

/**
 * Validate the country over a list of valid countries.
 * When country is not valid the corresponding input data must not be exported
 * @returns 
 */
function validateCountry() {
	// France, Guadeloupe, Guyane française, Polynésie française
	const validCountries = ['FR']
	return b => {
		if (!b || !validCountries.includes(b)) {
			throw ERRORMODULE.create({
				name: 'BWP_EXCLUDE_THIS_RESULT',
				message: `message: country ${b} is out of scope`
			});
		}
		return b;
	};
}

function replaceChars(charsToReplace, replacementChar) {
	let regex;
	if (charsToReplace instanceof RegExp) {
		regex = charsToReplace;
	} else {
		let reservedCharsRegEx = /([\.\?\+\*])/g;
		regex = new RegExp('[' + charsToReplace.replace(reservedCharsRegEx, '\\$1') + ']', 'g');
	}
	return b => b && b.toString().length > 0 ? b.toString().replace(regex, replacementChar) : b;
}

function checkMandatory() {
	return b => {
		if (!b) {
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: 'message: empty data not allowed'
			});
		}
		return b;
	};
}

function truncate(a) {
	return b => b && b.toString().length > a ? b.toString().trimStart().substring(0, a) : b;
}

function trimSpace() {
	return b => b && b.toString().length > 0 ? b.toString().trim() : b;
}

function mainFolderPath() { return 'Data Exports/Factor'; }
function workFolderPath() { return `${mainFolderPath()}/Work`; }
function errorFolderPath() { return `${mainFolderPath()}/Errors`; }

function retrieveFolderId(fullPath) {
	let regex = /\//g;
	let splitResult = fullPath.split(regex);
	let parentFolderId = 0;
	let folderId = 0;
	for (let i = 0 ; i < splitResult.length ; i++) {
		parentFolderId = folderId;
		let folderName = splitResult[i];
		let nameFilter = SEARCHMODULE.createFilter({
			name: 'name',
			operator: SEARCHMODULE.Operator.IS,
			values: [folderName]
		});
		let parentFilter;
		if (parentFolderId > 0) {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.IS,
				values: [parentFolderId]
			});
		} else {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.ISEMPTY
			});				
		}
		let folderSearch = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.FOLDER,
			title: 'Search Folder',
			columns: [{
				name: 'internalid'
			}, {
				name: 'parent'
			}],
			filters: [nameFilter, parentFilter]
		});
		let searchResult = folderSearch.run();
		let resultRange = searchResult.getRange({
			start: 0,
			end: 1
		});
		if (resultRange.length == 1) {
			folderId = resultRange[0].getValue({ name: 'internalid'});
		} else {
			folderId = 0;
			break;
		}
	}
	return parseInt(folderId, 10);
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);
	}
}

function logVar(value, title) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}