var ERRORMODULE, FILEMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE;

/**
 * This script is in charge to generate a temporary Customer_nnn file in Work forlder
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/error', 'N/file', 'N/record', 'N/runtime', 'N/search'], runMapReduce);

function runMapReduce(error, file, record, runtime, search) {
	ERRORMODULE= error;
	FILEMODULE= file;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	
	let returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
 * @param {Object} inputContext
 * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Object} inputContext.ObjectRef - Object that references the input data
 * @typedef {Object} ObjectRef
 * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
 * @property {string} ObjectRef.type - Type of the record instance that contains the input data
 * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
 * @since 2015.2
 */
function _getInputData(inputContext) {	
	log.debug('getInputData started');

	// Retrieve and validate scripts parameters
	const scriptParams = retrieveScriptParameters();
	if (!scriptParams.extractionId) {
		log.debug({
			title:'Process stopped',
			details: 'Extraction id is empty'
		});
		return;
	}
	
	log.debug({
		title:'Processing factor extration',
		details: scriptParams.extractionId
	});

	// Retrieve and validate data from factor extraction record 
	let factorRec = RECORDMODULE.load({
		type: 'customrecord_bwp_factorextraction',
		id: scriptParams.extractionId,
		isDynamic: false
	});
	const factorStep = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractstep' });
	if (factorStep != 3) {
		log.debug({
			title:'Process stopped',
			details: `Invalid factor step ${factorStep}`
		});
		return;
	}
    const factorMessage = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextracterrormsg' });
	
	log.debug('getInputData done');
    if (factorMessage) {
        return {};
    } else {
        return {
            type: 'file',
            path: retrieveInputFilePath(scriptParams.extractionId)
        };
    }
}

/**
 * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
 * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
 * context.
 * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
 *     is provided automatically based on the results of the getInputData stage.
 * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
 *     function on the current key-value pair
 * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
 *     pair
 * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} mapContext.key - Key to be processed during the map stage
 * @param {string} mapContext.value - Value to be processed during the map stage
 * @since 2015.2
 */
function _map(context) {
	//log.debug('map started');

    context.write({ 
		key: context.value,
		value: 1
	});
	
	//log.debug('map done');
}

/**
 * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
 * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
 * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
 *     provided automatically based on the results of the map stage.
 * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
 *     reduce function on the current group
 * @param {number} reduceContext.execut ionNo - Number of times the reduce function has been executed on the current group
 * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} reduceContext.key - Key to be processed during the reduce stage
 * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
 *     for processing
 * @since 2015.2
 */
function _reduce(context) {
	// log.debug('reduce started');

	// log.debug('reduce context', context);
	const scriptParams = retrieveScriptParameters();
    let values = context.key.split(scriptParams.fieldSeparator);
    let taggedValues = tagValues(values);
    taggedValues.extractionId = scriptParams.extractionId;
    processInputLine(taggedValues);

	// log.debug('reduce done');
}

/**
 * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
 * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
 * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
 * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
 *     script
 * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
 * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
 * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
 * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
 *     script
 * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
 * @param {Object} summaryContext.inputSummary - Statistics about the input stage
 * @param {Object} summaryContext.mapSummary - Statistics about the map stage
 * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
 * @since 2015.2
 */
function _summarize(summary) {
	log.debug('summary started');
//	log.debug('summary', summary);

	const scriptParams = retrieveScriptParameters();
	if (!scriptParams.extractionId) return;
	
	let factorRec = RECORDMODULE.load({
		type: 'customrecord_bwp_factorextraction',
		id: scriptParams.extractionId,
		isDynamic: false
	});
	let factorMessage = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextracterrormsg' });
	let customerFileId = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractcustfileid' });
	let balanceFileId = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractbalfileid' });
	let transactionFileId = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextracttranfileid' });
	let newCustomerFileId = 0;
	let newBalanceFileId = 0;
	let updateSubsidiaryDate;
	if (factorMessage) {
		// Nothing particular - just delete work files : this actions is performed below
	} else {
		if (writeErrorsToFlatFile(summary, scriptParams.extractionId) > 0) {
			let messageTitle = 'Errors on Finalization';
			let messageDetails = 'contact your administrator - see Errors folder';
			log.debug({
				title: messageTitle,
				details: messageDetails
			});
			factorRec.setValue({ 
				fieldId: 'custrecord_bwp_factorextracterrormsg',
				value: `${messageTitle} - ${messageDetails}`
			});
		} else {
			// Copy work files to target folder
			let targetFolderId = retrieveFolderId(mainFolderPath());
			let newCustomerFile = FILEMODULE.copy({ 
				folder: targetFolderId,
				id: customerFileId,
				conflictResolution: FILEMODULE.NameConflictResolution.OVERWRITE
			});
			log.debug({
				title: 'Process summary',
				details: '**********************************'
			});
			log.debug({
				title: 'File created',
				details: `${mainFolderPath()}/${newCustomerFile.name}`
			});
			let newBalanceFile = FILEMODULE.copy({ 
				folder: targetFolderId,
				id: balanceFileId,
				conflictResolution: FILEMODULE.NameConflictResolution.OVERWRITE
			});
			log.debug({
				title: 'File created',
				details: `${mainFolderPath()}/${newBalanceFile.name}`
			});
			log.debug({
				title: 'Process summary',
				details: '**********************************'
			});
			newCustomerFileId = newCustomerFile.id;
			newBalanceFileId = newBalanceFile.id;
			let createdDate = factorRec.getValue({ fieldId : 'created' });
			updateSubsidiaryDate = new Date(createdDate.getFullYear(), createdDate.getMonth(), createdDate.getDate(), 0, 0, 0);
		}
	}
	
	// Delete work files
	if (updateSubsidiaryDate) {
		[customerFileId, balanceFileId, transactionFileId].forEach(
			fileId => {
				if (fileId) {
					try {
						// Maybe the file does not exists
						let fileName = retrieveFileName(fileId);
						FILEMODULE.delete({ id: fileId });
						log.debug({
							title: 'File deleted',
							details: `${workFolderPath()}/${fileName}`
						});
					} catch (ex) { }
				}
			}
		)
		factorRec.setValue({ 
			fieldId: 'custrecord_bwp_factorextractcustfileid',
			value: newCustomerFileId
		});
		factorRec.setValue({ 
			fieldId: 'custrecord_bwp_factorextractbalfileid',
			value: newBalanceFileId
		});
		factorRec.setValue({ 
			fieldId: 'custrecord_bwp_factorextracttranfileid',
			value: 0
		});
	}
	
	// Update factor extraction record
	factorRec.setValue({ 
		fieldId: 'custrecord_bwp_factorextractstep',
		value: 4
	});
	factorRec.save();

	if (updateSubsidiaryDate) {
		let subsidiaryRecord = RECORDMODULE.load({
			type: RECORDMODULE.Type.SUBSIDIARY,
			id: factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractsubsidiary' }),
			isDynamic: false
		});
		subsidiaryRecord.setValue({
			fieldId: 'custrecord_bwp_factorextractdate',
			value: updateSubsidiaryDate
		});
		subsidiaryRecord.save();
	}
	
	log.debug('summary done');
}

function retrieveFileName(fileId) {
	let fileName = '';
	try {
		let fieldsValues = SEARCHMODULE.lookupFields({
			type: 'file',
			id: fileId,
			columns: [ 'name' ]
		});
		if ('name' in fieldsValues) {
			fileName = fieldsValues['name'];
		}
	} catch (ex) { }
	return fileName;
}

function mainFolderPath() { return 'Data Exports/Factor'; }
function workFolderPath() { return `${mainFolderPath()}/Work`; }
function errorFolderPath() { return `${mainFolderPath()}/Errors`; }


function retrieveInputFilePath(extractionId) {
    return `${workFolderPath()}/Transactions_${extractionId}`;
}

function writeErrorsToFlatFile(summary, extractionId) {
	let errorCount = 0;
	const fileName = `Finalyze_Errors_${extractionId}`;
	const folder = errorFolderPath();
	const folderId = retrieveFolderId(folder);
	let fileObj;
	summary.reduceSummary.errors.iterator().each(
		function(key, error, executionNo) {
			if (!fileObj) {
				fileObj = FILEMODULE.create({
					name: fileName,
					fileType: FILEMODULE.Type.PLAINTEXT,
					description: 'Finalize Factor Extract Errors',
					encoding: FILEMODULE.Encoding.UTF8,
					folder: folderId
				});
			}
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj.appendLine({
				value: message
			});
			errorCount++;
			// log.debug({
			// 	title: 'error',
			// 	details: error
			// });
			return true;
		}
	);
	if (fileObj) {
		fileObj.save();
		log.debug({
			title: 'File created',
			details: `${folder}/${fileName}`
		});
	}
	return errorCount;
}

function tagValues(values) {
	const tags = ['internalid', 'type', 'tranid'];
	let taggedValues = {};
	for (let i = 0 ; i < tags.length ; i++) {
		taggedValues[tags[i]] = values[i] || '';
	}
	return taggedValues;
}

function retrieveScriptParameters() {	
	let script = RUNTIMEMODULE.getCurrentScript();
	return {
		extractionId: script.getParameter({ name: 'custscript_bwp_mr_factorextractidfinal' }) || 59,
		fieldSeparator: script.getParameter({ name: 'custscript_bwp_mr_factorextractfieldseparator' }) || ';'
	}
}

/**
 * Map each value expected in the output with the corresponding value from the input
 * Set data processors that are in charge to validate and format the data to output
 * Data processors must be functions that return arrow function
 * @param {*} resultValues 
 * @returns {Object} outputData 
 * @returns {string} outputData.vendorCode
 * @returns {string} outputData.customerCode
 * @returns {string} outputData.customerIdentifier
 * @returns {string} outputData.customerAcronym
 * @returns {string} outputData.customerCompanyName
 * @returns {string} outputData.address
 * @returns {string} outputData.additionalAddress
 * @returns {string} outputData.postalCode
 * @returns {string} outputData.city
 * @returns {string} outputData.country
 * @returns {string} outputData.phone
 */
function mapData(resultValues) {
	// log.debug({
	// 	title: 'Function start',
	// 	details: 'mapData'
	// });
	let dataMap = {
		metadata: {
			sources: {
				transactionsFile: retrieveInputFilePath(resultValues.extractionId)
			}
		},
		fields: {
			internalid: {
				fieldSource: 'transactionsFile.internalid',
				sourceValue: resultValues['internalid'],
				dataProcessors: []
			},
			type: {
				fieldSource: 'transactionsFile.type',
				sourceValue: resultValues['type'],
				dataProcessors: [transcoType()]
			},
            custrecord_bwp_factorextractionnumber: {
				fieldSource: '',
				sourceValue: resultValues['extractionId'],
				dataProcessors: []
            },
            custrecord_bwp_invoice_factored: {
				fieldSource: '',
				sourceValue: true,
				dataProcessors: []
            }
		}
	};
	// logRecord('outputData', outputData);
	return dataMap;
}

function processInputLine(taggedValues) {
	// log.debug({
	// 	title: 'Function start',
	// 	details: 'processInputLine'
	// });
	let dataMap = mapData(taggedValues);
	applyFunctions(dataMap);

	// Submit fields fails ==> because of submit fields context particularity ==> load and save the transaction record
	// try {
	// 	RECORDMODULE.submitFields({
	// 		type: dataMap.fields['type'].destValue,
	// 		id: dataMap.fields['internalid'].destValue,
	// 		values: generateSubmitFieldsValues(dataMap)
	// 	});
	// } catch(ex) {
	// 	log.debug({
	// 		title: 'Transaction update failed',
	// 		details: `${taggedValues['tranid']} (${taggedValues['internalid']}) - Type ${dataMap.fields['type'].destValue}`
	// 	});
	// 	logVar('submitFields values', generateSubmitFieldsValues(dataMap));
	// 	throw ex;
	// }

	let transacexportRecord;
	let searchObj = SEARCHMODULE.create({
		type: 'customrecord_bwp_transactionexports',
		filters: [['custrecord_bwp_transactionexportstransac', 'anyof', dataMap.fields['internalid'].destValue]] ,
		columns: ['internalid']
	});
	let results = searchObj.run().getRange({ start: 0, end: 1 });
	if (results.length > 0) {
		transacexportRecord = RECORDMODULE.load({
			type: 'customrecord_bwp_transactionexports',
			id: results[0].getValue({ name: 'internalid' }),
			isDynamic: false
		});
	} else {
		transacexportRecord = RECORDMODULE.create({
			type: 'customrecord_bwp_transactionexports',
			isDynamic: false
		});
		transacexportRecord.setValue({
			fieldId: 'custrecord_bwp_transactionexportstransac',
			value: dataMap.fields['internalid'].destValue
		});
	}

	transacexportRecord.setValue({
		fieldId: 'custrecord_bwp_factorextractionnumber',
		value: dataMap.fields['custrecord_bwp_factorextractionnumber'].destValue
	});
	transacexportRecord.setValue({
		fieldId: 'custrecord_bwp_invoice_factored',
		value: dataMap.fields['custrecord_bwp_invoice_factored'].destValue
	});
	try {
		transacexportRecord.save();
	} catch(ex) {
		log.debug({
			title: 'Transaction update failed',
			details: `${taggedValues['tranid']} (${taggedValues['internalid']}) - Type ${dataMap.fields['type'].destValue}`
		});
		throw ex;
	}

	log.debug({
		title: 'Transaction update',
		details: `${taggedValues['tranid']} (${taggedValues['internalid']})`
	});
	return dataMap;
}

function generateSubmitFieldsValues(dataMap) {
    let submitFieldsValues = {};
	let fields = dataMap.fields;
    for (let key in fields) {
        if (key != 'internalid' && key != 'type' && fields.hasOwnProperty(key)) {
            submitFieldsValues[key] = fields[key].destValue;
        }
    }
	return submitFieldsValues;
}

function applyFunctions(dataMap) {
	// log.debug({
	// 	title: 'Function start',
	// 	details: 'applyFunctions'
	// });
	let fields = dataMap.fields;
	for (let key in fields) {
		try {
			fields[key].destValue = applyFieldFunctions(fields[key].sourceValue, [...(fields[key].dataProcessors || [])]);
		} catch (ex) {
			if (ex.name && ex.name.startsWith('BWP_') && ex.message) {
				const regex = /(^\w+)\.(.+)/;
				let regexRes = regex.exec(fields[key].fieldSource);
				let source = '';
				let field = '';
				let sourceInfo = '';
				if (regexRes) {
					source = regexRes[1];
					field = regexRes[2];
					sourceInfo = outputData.metadata.sources[source];
				}
				let message = `${source} ${sourceInfo} - field ${field} - ${ex.message}`;
				if (ex.name == 'BWP_EXCLUDE_THIS_RESULT') {
					// This result may not be exported ==> reset outputValues and exit
					outputValues = [];
					log.debug({
						title: 'Data exclusion',
						details: message
					});
					break;
				}
				throw ERRORMODULE.create({
					name: ex.name,
					message: message,
					notifyOff: true
				});
			}
			throw ex;
		}
	}
}

/**
 * apply data processors functions in a recursive way
 * @param {*} fieldValue 
 * @param {*} functions 
 * @returns 
 */
function applyFieldFunctions(fieldValue, functions) {
	if (functions.length == 0) {
		return fieldValue;
	}
	let func = functions.pop();
	if (!(typeof func === 'function')) {
		func = a => a;
		log.debug('not a function');
	}
	return applyFieldFunctions(func(fieldValue), functions);
}

function transcoType() {	
	const transcoTypes = {
		CUSTCRED: RECORDMODULE.Type.CREDIT_MEMO,
		CUSTINVC: RECORDMODULE.Type.INVOICE,
		CUSTPYMT: RECORDMODULE.Type.CUSTOMER_PAYMENT,
		JOURNAL: RECORDMODULE.Type.JOURNAL_ENTRY
	};
	return b => transcoTypes[b.toUpperCase()];
}

function retrieveFolderId(fullPath) {
	let regex = /\//g;
	let splitResult = fullPath.split(regex);
	let parentFolderId = 0;
	let folderId = 0;
	for (let i = 0 ; i < splitResult.length ; i++) {
		parentFolderId = folderId;
		let folderName = splitResult[i];
		let nameFilter = SEARCHMODULE.createFilter({
			name: 'name',
			operator: SEARCHMODULE.Operator.IS,
			values: [folderName]
		});
		let parentFilter;
		if (parentFolderId > 0) {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.IS,
				values: [parentFolderId]
			});
		} else {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.ISEMPTY
			});				
		}
		let folderSearch = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.FOLDER,
			title: 'Search Folder',
			columns: [{
				name: 'internalid'
			}, {
				name: 'parent'
			}],
			filters: [nameFilter, parentFilter]
		});
		let searchResult = folderSearch.run();
		let resultRange = searchResult.getRange({
			start: 0,
			end: 1
		});
		if (resultRange.length == 1) {
			folderId = resultRange[0].getValue({ name: 'internalid'});
		} else {
			folderId = 0;
			break;
		}
	}
	return parseInt(folderId, 10);
}

function logRecord(logTitle, record) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}