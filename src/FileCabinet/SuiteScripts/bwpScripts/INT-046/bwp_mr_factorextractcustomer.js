var ERRORMODULE, FILEMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE;

/**
 * This script is in charge to generate a temporary Customer_nnn file in Work forlder
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/error', 'N/file', 'N/record', 'N/runtime', 'N/search'], runMapReduce);

function runMapReduce(error, file, record, runtime, search) {
	ERRORMODULE= error;
	FILEMODULE= file;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	
	let returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	// returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
 * @param {Object} inputContext
 * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Object} inputContext.ObjectRef - Object that references the input data
 * @typedef {Object} ObjectRef
 * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
 * @property {string} ObjectRef.type - Type of the record instance that contains the input data
 * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
 * @since 2015.2
 */
function _getInputData(inputContext) {	
	log.debug('getInputData started');

	// Retrieve and validate scripts parameters
	const scriptParams = retireveScriptParameters();
	if (!scriptParams.extractionId) {
		log.debug({
			title:'Process stopped',
			details: 'Extraction id is empty'
		});
		return;
	}
	
	log.debug({
		title:'Processing factor extration',
		details: scriptParams.extractionId
	});

	// Retrieve and validate data from factor extraction record 
	let factorRec = RECORDMODULE.load({
		type: 'customrecord_bwp_factorextraction',
		id: scriptParams.extractionId,
		isDynamic: false
	});
	// logRecord('factorRec', factorRec);
	const factorStep = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractstep' });
	if (factorStep != 1) {
		log.debug({
			title:'Process stopped',
			details: `Invalid factor step ${factorStep}`
		});
		return;
	}
	const subsidiary = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractsubsidiary' });
	log.debug({
		title:'Subsidiary',
		details: `${subsidiary} - ${factorRec.getText({ fieldId: 'custrecord_bwp_factorextractsubsidiary' })}`
	});

	// // Retrieve internal id for value 'Financeur' of custom list customlist_bwp_typologie_client
	// let customerTypesSearchObj = SEARCHMODULE.create({
	// 	type: 'customlist_bwp_typologie_client',
	// 	filters: [['name', 'is', 'Financeur']],
	// 	columns: ['internalid']
	// });
	// const fincanceurInternalId = customerTypesSearchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid'});

	let suiteQLQuery = `Select t3.id as customer_id, t3.entitytitle, t2.id as subsidiary_id, t2.fullname
			, t2.custrecord_bwp_code_vendeur_cedant, t3.entityid, t3.custentity_bwp_siret, t3.companyname
			, t4.addr1, t4.addr2, t4.zip, t4.city, t4.country, t3.phone
		From CustomerSubsidiaryRelationship t1
			Inner Join subsidiary t2
			  On t2.id = t1.subsidiary
			Inner Join customer t3
			  On t3.id = t1.entity
			Left Outer Join EntityAddress t4
			  On t4.nkey = t3.defaultBillingAddress
		Where t3.custentity_bwp_factor = 'T'
	  	  And t1.subsidiary = ${subsidiary}`;
	
	// suiteQLQuery += `And t3.id = 26935`;
	
	log.debug('getInputData done');
	return {
		type: 'suiteql',
		query: suiteQLQuery,
	};
}

function tagValues(values) {
	// logRecord(values, 'values');
	const tags = ['customer_id', 'entitytitle', 'subsidiary_id', 'subsidiary_fullname', 'custrecord_bwp_code_vendeur_cedant', 
		'entityid', 'custentity_bwp_siret', 'companyname', 'addr1', 'addr2', 'zip', 'city', 'country', 'phone'];
	let taggedValues = {};
	for (let i = 0 ; i < tags.length ; i++) {
		taggedValues[tags[i]] = values[i];
	}
	return taggedValues;
}

/**
 * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
 * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
 * context.
 * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
 *     is provided automatically based on the results of the getInputData stage.
 * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
 *     function on the current key-value pair
 * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
 *     pair
 * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} mapContext.key - Key to be processed during the map stage
 * @param {string} mapContext.value - Value to be processed during the map stage
 * @since 2015.2
 */
function _map(context) {
	//log.debug('map started');
	// logRecord('map context value', context.value);
	let values = JSON.parse(context.value).values;
	let taggedValues = tagValues(values);
	// logRecord('taggedValues', taggedValues);

	let writeContext = generateOutputLine(taggedValues);

	if (writeContext.value) {
		context.write(writeContext);

		// // Tag extracted customer
		// const scriptParams = retireveScriptParameters();
		// const custSubsidRelation = resultValues.internalid;
		// try {
		// 	RECORDMODULE.submitFields({
		// 		type: RECORDMODULE.Type.CUSTOMER_SUBSIDIARY_RELATIONSHIP,
		// 		id: custSubsidRelation,
		// 		values: { custrecord_bwp_factorextractnumber: scriptParams.extractionId }
		// 	});
		// } catch (ex) {
		// 	throw ERRORMODULE.create({
		// 		name: `Failed to submitFields on CUSTOMER_SUBSIDIARY_RELATIONSHIP record ${custSubsidRelation} 
		// 			(subsidiary: ${resultValues.subsidiary.value} - customer: ${resultValues.entity.value})`,
		// 		message: ex.message
		// 	});
		// }
	}
	
	//log.debug('map done');
}

/**
 * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
 * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
 * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
 *     provided automatically based on the results of the map stage.
 * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
 *     reduce function on the current group
 * @param {number} reduceContext.execut ionNo - Number of times the reduce function has been executed on the current group
 * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} reduceContext.key - Key to be processed during the reduce stage
 * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
 *     for processing
 * @since 2015.2
 */
function _reduce(context) {
//	log.debug('reduce started');
	
	//log.debug('reduce done');
}

/**
 * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
 * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
 * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
 * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
 *     script
 * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
 * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
 * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
 * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
 *     script
 * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
 * @param {Object} summaryContext.inputSummary - Statistics about the input stage
 * @param {Object} summaryContext.mapSummary - Statistics about the map stage
 * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
 * @since 2015.2
 */
function _summarize(summary) {
	log.debug('summary started');
//	log.debug('summary', summary);

	const scriptParams = retireveScriptParameters();
	if (!scriptParams.extractionId) return;

	if (writeErrorsToFlatFile(summary, scriptParams.extractionId) == 0) {
		let fileId = writeDataToFlatFile(summary, scriptParams.extractionId);
		RECORDMODULE.submitFields({
			type: 'customrecord_bwp_factorextraction',
			id: scriptParams.extractionId,
			values: { 
				custrecord_bwp_factorextractstep: 2,
				custrecord_bwp_factorextractcustfileid: fileId
			}
		});
	} else {
		RECORDMODULE.submitFields({
			type: 'customrecord_bwp_factorextraction',
			id: scriptParams.extractionId,
			values: { 
				custrecord_bwp_factorextracterrormsg: 'Errors on Customer Extraction - see error file for details',
				custrecord_bwp_factorextractstep: 3
			}
		});
	}
	
	log.debug('summary done');
}

function writeErrorsToFlatFile(summary, extractionId) {
	let errorCount = 0;
	const fileName = `Customer_Errors_${extractionId}`;
	const folder = errorFolderPath();
	const folderId = retrieveFolderId(folder);
	let fileObj;
	summary.mapSummary.errors.iterator().each(
		function(key, error, executionNo) {
			if (!fileObj) {
				fileObj = FILEMODULE.create({
					name: fileName,
					fileType: FILEMODULE.Type.PLAINTEXT,
					description: 'Customer Factor Extract Errors',
					encoding: FILEMODULE.Encoding.UTF8,
					folder: folderId
				});
			}
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj.appendLine({
				value: message
			});
			errorCount++;
			// log.debug({
			// 	title: 'error',
			// 	details: error
			// });
			return true;
		}
	);
	if (fileObj) {
		fileObj.save();
		log.debug({
			title: 'File created',
			details: `${folder}/${fileName}`
		});
	}
	return errorCount;
}

function writeDataToFlatFile(summary, extractionId) {
	let fileId = 0;
	let factorRec = RECORDMODULE.load({
		type: 'customrecord_bwp_factorextraction',
		id: extractionId,
		isDynamic: false
	});
	const timestamp = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextracttimestamp' });

	// Generate first and last lines based on subsidiary's information
	let subsidiaryRec = RECORDMODULE.load({
		type: RECORDMODULE.Type.SUBSIDIARY,
		id: factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractsubsidiary' }),
		isDynamic: false
	});
	const codeVendeurCedant = subsidiaryRec.getValue({ fieldId: 'custrecord_bwp_code_vendeur_cedant' });

	let firstLine;
	let lastLine;
	try {
		firstLine = generateFirstLastOutputLine(subsidiaryRec, true);
		lastLine = generateFirstLastOutputLine(subsidiaryRec, false);
	} catch (ex) {
		log.debug({
			title: 'error',
			details: ex
		});
	}

	// Set temporary filename and write data
	// const fileName = `Customer_${extractionId}`;
	const fileName = `${codeVendeurCedant} - Export_Clients du ${timestamp}`;
	const folder = workFolderPath();
	const folderId = retrieveFolderId(folder);
	let fileObj;

	summary.output.iterator().each((key, value) => {
		switch (key) {
			case 'OutputLine':
				// log.debug({
				// 	title: key,
				// 	details: value
				// });
				if (!fileObj) {
					try {
						let oldFile = FILEMODULE.load({
							id: `${folder}/${fileName}`
						});
						FILEMODULE.delete({ id: oldFile.id });
					} catch (ex) { }
					fileObj = FILEMODULE.create({
						name: fileName,
						fileType: FILEMODULE.Type.PLAINTEXT,
						description: 'Customer Factor Extract ' + extractionId,
						encoding: FILEMODULE.Encoding.WINDOWS_1252,  // UTF8 does not work
						folder: folderId
					});
					fileObj.appendLine({
						value: `${firstLine}\r`
					});
				}
				fileObj.appendLine({
					value: `${value}\r`
				});
				break;
		}
		return true;
	});
	if (fileObj) {
		fileObj.appendLine({
			value: `${lastLine}\r`
		});
		fileId = fileObj.save();
		log.debug({
			title: 'File created',
			details: `${folder}/${fileName}`
		});
	}
	return fileId;
}

function retireveScriptParameters() {	
	let script = RUNTIMEMODULE.getCurrentScript();
	return {
		extractionId: script.getParameter({ name: 'custscript_bwp_mr_factorextractidcust' }) || 1,
		fieldSeparator: script.getParameter({ name: 'custscript_bwp_mr_factorextractfieldseparator' }) || ';'
	}
}

function getCustomerAddress(customer) {
	let recordObj = RECORDMODULE.load({
		type: RECORDMODULE.Type.CUSTOMER,
		id: customer,
		isDynamic: false
	});
	let line = recordObj.findSublistLineWithValue({
		sublistId: 'addressbook',
		fieldId: 'defaultbilling',
		value: true
	});
	if (line >= 0) {
		return recordObj.getSublistSubrecord({ 
			sublistId: 'addressbook',
			fieldId: 'addressbookaddress',
			line: line
		});
	} else {
		log.debug({
			title: `customer ${customer}`,
			details: 'no billing address for this customer' 
		})
		return RECORDMODULE.create({
			type: 'address'
		});
	}
}

function generateOutputLine(resultValues) {
	return { 
		key: 'OutputLine',
		value: formatOutputLine(mapData(resultValues))
	};
}

function generateFirstLastOutputLine(subsidiaryRec, first) {
	return formatOutputLine(mapFisrtOrLastLine(subsidiaryRec, first));
}

/**
 * Map each value expected in the output with the corresponding value from the input
 * Set data processors that are in charge to validate and format the data to output
 * Data processors must be functions that return arrow function
 * @param {*} resultValues 
 * @returns {Object} outputData 
 * @returns {string} outputData.vendorCode
 * @returns {string} outputData.customerCode
 * @returns {string} outputData.customerIdentifier
 * @returns {string} outputData.customerAcronym
 * @returns {string} outputData.customerCompanyName
 * @returns {string} outputData.address
 * @returns {string} outputData.additionalAddress
 * @returns {string} outputData.postalCode
 * @returns {string} outputData.city
 * @returns {string} outputData.country
 * @returns {string} outputData.phone
 */
function mapData(resultValues) {
	let outputData = {
		metadata: {
			sources: {
				subsidiary: `${resultValues.subsidiary_fullname} (${resultValues.subsidiary_id})`,
				customer: `${resultValues.entitytitle} (${resultValues.customer_id})`
			}
		},
		fields: {
			vendorCode: {
				fieldSource: 'subsidiary.custrecord_bwp_code_vendeur_cedant',
				sourceValue: resultValues['custrecord_bwp_code_vendeur_cedant'],
				dataProcessors: [checkMandatory(), truncate(6)]
			},
			customerCode: {
				fieldSource: 'customer.entityid',
				sourceValue: resultValues['entityid'],
				dataProcessors: [checkMandatory(), truncate(10)]
			},
			customerIdentifier: {
				fieldSource: 'customer.custentity_bwp_siret',
				sourceValue: resultValues['custentity_bwp_siret'],
				dataProcessors: [checkMandatory(), truncate(14)]
			},
			customerAcronym: {
				fieldSource: 'customer.companyname',
				sourceValue: resultValues['companyname'],
				dataProcessors: [checkMandatory(), truncate(40)]
			},
			customerCompanyName: {
				fieldSource: 'customer.companyname',
				sourceValue: resultValues['companyname'],
				dataProcessors: [checkMandatory(), truncate(40)]
			},
			address: {
				fieldSource: 'customer.address.addr1',
				sourceValue: resultValues['addr1'],
				dataProcessors: [checkMandatory(), truncate(40)]
			},
			additionalAddress: {
				fieldSource: 'customer.address.addr2',
				sourceValue: resultValues['addr2'],
				dataProcessors: [truncate(40)]
			},
			postalCode: {
				fieldSource: 'customer.address.zip',
				sourceValue: resultValues['zip'],
				dataProcessors: [checkMandatory(), truncate(6)]
			},
			city: {
				fieldSource: 'customer.address.city',
				sourceValue: resultValues['city'],
				dataProcessors: [checkMandatory(), truncate(34)]
			},
			// TODO: Process address country first because it may result in a data exclusion --> it is useless to test other address fields
			country: {
				fieldSource: 'customer.address.country',
				sourceValue: resultValues['country'],
				dataProcessors: [checkMandatory(), truncate(3), validateCountry()]
			},
			phone: {
				fieldSource: 'customer.phone',
				sourceValue: resultValues['phone'],
				dataProcessors: [truncate(10), replaceChars(/[^\d]/g, '')]
			}
		}
	};
	// logRecord('outputData', outputData);
	return outputData;
}

/**
 * The output file contains a header line and a footer line. They contain hardcoded values and information coming from the concerned subsidiary
 * @param {*} subsidiaryRec 
 * @param {*} first 
 * @returns {Object} see mapData
 */
function mapFisrtOrLastLine(subsidiaryRec, first) {
	let address = subsidiaryRec.getSubrecord({ fieldId: 'mainaddress'});
	let outputData = {
		metadata: {
			sources: {
				subsidiary: `${subsidiaryRec.getValue({ fieldId: 'name'})} (${subsidiaryRec.id})`
			}
		},
		fields: {
			vendorCode: {
				fieldSource: '',
				sourceValue: first ? '000000' : '999999',
				dataProcessors: [checkMandatory(), truncate(6)]
			},
			customerCode: {
				fieldSource: '',
				sourceValue: first ? 'DEB' : 'FIN',
				dataProcessors: [checkMandatory(), truncate(10)]
			},
			customerIdentifier: {
				fieldSource: 'subsidiary.federalidnumber',
				sourceValue: subsidiaryRec.getValue({ fieldId: 'federalidnumber'}),
				dataProcessors: [checkMandatory(), truncate(14)]
			},
			customerAcronym: {
				fieldSource: 'subsidiary.name',
				sourceValue: subsidiaryRec.getValue({ fieldId: 'name'}),
				dataProcessors: [checkMandatory(), truncate(40)]
			},
			customerCompanyName: {
				fieldSource: 'subsidiary.name',
				sourceValue: subsidiaryRec.getValue({ fieldId: 'name'}),
				dataProcessors: [checkMandatory(), truncate(40)]
			},
			address: {
				fieldSource: 'subsidiary.address.addr1',
				sourceValue: address.getValue({ fieldId: 'addr1'}),
				dataProcessors: [checkMandatory(), truncate(40)]
			},
			additionalAddress: {
				fieldSource: 'subsidiary.address.addr2',
				sourceValue: address.getValue({ fieldId: 'addr2'}),
				dataProcessors: [truncate(40)]
			},
			postalCode: {
				fieldSource: 'subsidiary.address.zip',
				sourceValue: address.getValue({ fieldId: 'zip'}),
				dataProcessors: [checkMandatory(), truncate(6)]
			},
			city: {
				fieldSource: 'subsidiary.address.city',
				sourceValue: address.getValue({ fieldId: 'city'}),
				dataProcessors: [checkMandatory(), truncate(34)]
			},
			country: {
				fieldSource: 'subsidiary.address.country',
				sourceValue: address.getValue({ fieldId: 'country'}),
				dataProcessors: [checkMandatory(), truncate(3), validateCountry()]
			},
			phone: {
				fieldSource: 'subsidiary.custrecord_bwp_telephone',
				sourceValue: subsidiaryRec.getValue({ fieldId: 'custrecord_bwp_telephone'}),
				dataProcessors: [truncate(10), replaceChars(/[^\d]/g, '')]
			}
		}
	};
	return outputData;
}

/**
 * Iterate over fields to ouput. Complete the list of data processors and apply them.
 * Handle formatting / validation exceptions (errors or data to exclude)
 * @param {*} outputData 
 * @returns 
 */
function formatOutputLine(outputData) {
	const scriptParams = retireveScriptParameters();
	// Set a shorter constant for readibility
	const sep = scriptParams.fieldSeparator;
	applyFunctions(outputData);
	let outputValues = [];
	let fields = outputData.fields;
	// // for testing purpose
	// fields.vendorCode.fieldValue = '';
	for (let key in fields) {
		outputValues.push(replaceChars(sep, ' ')(fields[key].destValue));
	}
	return outputValues.join(sep);
}

function applyFunctions(dataMap) {
	// log.debug({
	// 	title: 'Function start',
	// 	details: 'applyFunctions'
	// });
	const scriptParams = retireveScriptParameters();
	// Set a shorter constant for readibility
	const sep = scriptParams.fieldSeparator;

	let fields = dataMap.fields;
	for (let key in fields) {
		try {
			fields[key].destValue = applyFieldFunctions(fields[key].sourceValue, [...(fields[key].dataProcessors || [])]);
		} catch (ex) {
			if (ex.name && ex.name.startsWith('BWP_') && ex.message) {
				const regex = /(^\w+)\.(.+)/;
				let regexRes = regex.exec(fields[key].fieldSource);
				let source = '';
				let field = '';
				let sourceInfo = '';
				if (regexRes) {
					source = regexRes[1];
					field = regexRes[2];
					sourceInfo = dataMap.metadata.sources[source];
				}
				let message = `${source} ${sourceInfo} - field ${field} - ${ex.message}`;
				if (ex.name == 'BWP_EXCLUDE_THIS_RESULT') {
					// This result may not be extracted ==> reset fields
					dataMap.fields = {};
					log.debug({
						title: 'Data exclusion',
						details: message
					});
					break;
				}
				throw ERRORMODULE.create({
					name: ex.name,
					message: message,
					notifyOff: true
				});
			}
			throw ex;
		}
	}
}

/**
 * apply data processors functions in a recursive way
 * @param {*} stringValue 
 * @param {*} functions 
 * @returns 
 */
function applyFieldFunctions(fieldValue, functions) {
	if (functions.length == 0) {
		return fieldValue;
	}
	let func = functions.pop();
	if (!(typeof func === 'function')) {
		func = a => a;
		log.debug('not a function');
	}
	return applyFieldFunctions(func(fieldValue), functions);
}

/**
 * Validate the country over a list of valid countries.
 * When country is not valid the corresponding input data must not be exported
 * @returns 
 */
function validateCountry() {
	// France, Guadeloupe, Guyane française, Réunion, Martinique, Mayotte
	const validCountries = ['FR', 'GP', 'GF', 'RE', 'MQ', 'YT']
	return b => {
		if (!validCountries.includes(b)) {
			throw ERRORMODULE.create({
				name: 'BWP_EXCLUDE_THIS_RESULT',
				message: `message: country ${b} is out of scope`
			});
		}
		return b;
	};
}

function replaceChars(charsToReplace, replacementChar) {
	let regex;
	if (charsToReplace instanceof RegExp) {
		regex = charsToReplace;
	} else {
		let reservedCharsRegEx = /([\.\?\+\*])/g;
		regex = new RegExp('[' + charsToReplace.replace(reservedCharsRegEx, '\\$1') + ']', 'g');
	}
	return b => b && b.toString().length > 0 ? b.toString().replace(regex, replacementChar) : b;
}

function checkMandatory() {
	return b => {
		if (!b) {
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: 'message: empty data not allowed'
			});
		}
		return b;
	};
}

function truncate(a) {
	return b => b && b.toString().length > a ? b.toString().trimStart().substring(0, a) : b;
}

function trimSpace() {
	return b => b && b.toString().length > 0 ? b.toString().trim() : b;
}

function mainFolderPath() { return 'Data Exports/Factor'; }
function workFolderPath() { return `${mainFolderPath()}/Work`; }
function errorFolderPath() { return `${mainFolderPath()}/Errors`; }

function retrieveFolderId(fullPath) {
	let regex = /\//g;
	let splitResult = fullPath.split(regex);
	let parentFolderId = 0;
	let folderId = 0;
	for (let i = 0 ; i < splitResult.length ; i++) {
		parentFolderId = folderId;
		let folderName = splitResult[i];
		let nameFilter = SEARCHMODULE.createFilter({
			name: 'name',
			operator: SEARCHMODULE.Operator.IS,
			values: [folderName]
		});
		let parentFilter;
		if (parentFolderId > 0) {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.IS,
				values: [parentFolderId]
			});
		} else {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.ISEMPTY
			});				
		}
		let folderSearch = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.FOLDER,
			title: 'Search Folder',
			columns: [{
				name: 'internalid'
			}, {
				name: 'parent'
			}],
			filters: [nameFilter, parentFilter]
		});
		let searchResult = folderSearch.run();
		let resultRange = searchResult.getRange({
			start: 0,
			end: 1
		});
		if (resultRange.length == 1) {
			folderId = resultRange[0].getValue({ name: 'internalid'});
		} else {
			folderId = 0;
			break;
		}
	}
	return parseInt(folderId, 10);
}

function logRecord(logTitle, record) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}