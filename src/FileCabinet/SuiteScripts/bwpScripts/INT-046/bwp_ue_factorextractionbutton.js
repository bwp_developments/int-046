var SEARCHMODULE, UISERVERWIDGETMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/search', 'N/ui/serverWidget'], runUserEvent);

function runUserEvent(search, serverWidget) {
	SEARCHMODULE= search;
	UISERVERWIDGETMODULE= serverWidget;
	
	let returnObj = {};
	returnObj['beforeLoad'] = _beforeLoad;
	// returnObj['beforeSubmit'] = _beforeSubmit;
	// returnObj['afterSubmit'] = _afterSubmit;
	return returnObj;
}
   
/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {string} scriptContext.type - Trigger type
 * @param {Form} scriptContext.form - Current form
 * @Since 2015.2
 */
function _beforeLoad(scriptContext) {
	log.debug('beforeLoad started');	
	
	if (scriptContext.request && scriptContext.type == scriptContext.UserEventType.VIEW) {
		const vatRegistrNo = scriptContext.newRecord.getValue({ fieldId:'federalidnumber' });
		if (vatRegistrNo) {
			let stillRunning = false;

			scriptContext.form.clientScriptModulePath = './bwp_cs_factorextractionbutton.js';
			let factorButton = scriptContext.form.addButton({
				id: 'custpage_bwp_launchfactorextraction',
				label: 'Factor',
				functionName: 'launchFactorExtraction(' + scriptContext.newRecord.id + ')'
			});

			let extractionId = scriptContext.newRecord.getValue({ fieldId: 'custrecord_bwp_subsidfactorextractnumber'});
			if (extractionId) {
				if (extractionId == -1) {
					stillRunning = true;
				} else {
					try {
						let fieldsValues = SEARCHMODULE.lookupFields({
							type: 'customrecord_bwp_factorextraction',
							id: extractionId,
							columns: ['custrecord_bwp_factorextractstep']
						});
						if ('custrecord_bwp_factorextractstep' in fieldsValues) {
							let extractionStep = fieldsValues['custrecord_bwp_factorextractstep'] || 0;
							if (extractionStep != 4) {
								stillRunning = true;
							}
						}
					} catch (ex) {
						// Maybe the factorextraction record has been deleted (RCRD_DSNT_EXIST error)
						// ==> nothing to do
					}
				}
			}
			if (stillRunning) {
				log.debug('Still running');
				factorButton.isDisabled = true;
				factorButton.label = 'Factor running ...';
			}
		}
	}
	
	log.debug('beforeLoad done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _beforeSubmit(scriptContext) {
	log.debug('beforeSubmit started');
	
	log.debug('beforeSubmit done');
}

/**
 * Function definition to be triggered before record is loaded.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.type - Trigger type
 * @Since 2015.2
 */
function _afterSubmit(scriptContext) {	
	log.debug('afterSubmit started');
	
	log.debug('afterSubmit done');
}

function logRecord(logTitle, record) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}