var FORMATMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, WORKFLOWMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/record', 'N/runtime', 'N/search', 'N/workflow'], runSuitelet);

function runSuitelet(format, record, runtime, search, workflow) {
	FORMATMODULE= format;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
    WORKFLOWMODULE= workflow;
	let returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;	
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
	let method = context.request.method;
	//logRecord(context, 'context');
	
	log.debug(method);
	if (method == 'GET') {
		getFunction(context);
	} else if (method == 'POST') {
		postFunction(context);
	}
}

function getFunction(context) {
	log.debug({
        title: 'Function start',
        details: 'getFunction'
    });

	let subsidiary = context.request.parameters.custparam_bwp_subsidiary;
	let subsidiaryRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.SUBSIDIARY,
		id: subsidiary,
		isDynamic: false
	});

	// Check if extract process is currently running
	let extractionId = subsidiaryRecord.getValue({ fieldId: 'custrecord_bwp_subsidfactorextractnumber'});
	let stillRunning = false;
	if (extractionId) {
		if (extractionId == -1) {
			stillRunning = true;
		} else {
			try {
				let fieldsValues = SEARCHMODULE.lookupFields({
					type: 'customrecord_bwp_factorextraction',
					id: extractionId,
					columns: ['custrecord_bwp_factorextractstep']
				});
				if ('custrecord_bwp_factorextractstep' in fieldsValues) {
					let extractionStep = fieldsValues['custrecord_bwp_factorextractstep'] || 0;
					if (extractionStep != 4) {
						stillRunning = true;
					}
				}
			} catch (ex) {
				// Maybe the factorextraction record has been deleted (RCRD_DSNT_EXIST error)
				// ==> nothing to do
			}
		}
	}
	if (stillRunning) {
		log.debug('Process still running');
		return;
	}
	subsidiaryRecord.setValue({
		fieldId: 'custrecord_bwp_subsidfactorextractnumber',
		value: -1
	});
	subsidiaryRecord.save();

    let factorRecord = RECORDMODULE.create({
        type: 'customrecord_bwp_factorextraction',
        isDynamic: true
    });
    factorRecord.setValue({
        fieldId: 'custrecord_bwp_factorextractsubsidiary',
        value: subsidiary
    });
    factorRecord.setValue({
        fieldId: 'custrecord_bwp_factorextractstep',
        value: 0
    });
    factorRecord.setValue({
        fieldId: 'custrecord_bwp_factorextracttimestamp',
        value: userNowDateDDMMYYYY()
    });
    factorRecord.setValue({
        fieldId: 'custrecord_bwp_factorextractfieldsep',
        value: ';'
    });
    let internalId = parseInt(factorRecord.save(), 10);
	log.debug({
		title: 'Factor Record',
		details: `Record with id ${internalId} is created`
	});

	// RECORDMODULE.submitFields({
	// 	type: RECORDMODULE.Type.SUBSIDIARY,
	// 	id: subsidiary,
	// 	values: { custrecord_bwp_subsidfactorextractnumber: internalId }
	// });

	subsidiaryRecord = RECORDMODULE.load({
		type: RECORDMODULE.Type.SUBSIDIARY,
		id: subsidiary,
		isDynamic: false
	});
	subsidiaryRecord.setValue({
		fieldId: 'custrecord_bwp_subsidfactorextractnumber',
		value: internalId
	});
	subsidiaryRecord.save();

    WORKFLOWMODULE.initiate({
        recordId: internalId,
        recordType: 'customrecord_bwp_factorextraction',
        workflowId: 'customworkflow_bwp_factorextraction'
    });
    
	// if (error) {
	// 	context.response.write(JSON.stringify(error));
	// }
	
	log.debug({
        title: 'Function end',
        details: 'getFunction'
    });



	return internalId;
}

function postFunction(context) {

}

/**
 * 
 * @returns todays date in DDMMYYYY format based on local date
 */
 function userNowDateDDMMYYYY() {
	const userDateNow = FORMATMODULE.parse({
		value: FORMATMODULE.format({
			value: new Date(),
			type: FORMATMODULE.Type.DATETIME,
			timezone: RUNTIMEMODULE.getCurrentUser().getPreference({ name: 'timezone' })
		}),
		type: FORMATMODULE.Type.DATETIME,
		timezone: FORMATMODULE.Timezone.GMT
	});

	return `${userDateNow.getUTCDate().toString().padStart(2, '0')}${(userDateNow.getUTCMonth()+1).toString().padStart(2, '0')}${userDateNow.getUTCFullYear()}`;

	const dateNow = new Date();
	return `${dateNow.getDate().toString().padStart(2, '0')}${(dateNow.getMonth()+1).toString().padStart(2, '0')}${dateNow.getFullYear()}`;
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(value, title) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}