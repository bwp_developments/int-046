/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(["require", "exports"], function (require, exports) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.saveRecord = exports.fieldChanged = exports.pageInit = void 0;
    let pageInit = (scriptContext) => {
        let currentRec = scriptContext.currentRecord;
        let excludeChecked = currentRec.findSublistLineWithValue({
            sublistId: 'line',
            fieldId: 'custcol_bwp_exclude_factor',
            value: 'T',
        });
        let excludeUnchecked = currentRec.findSublistLineWithValue({
            sublistId: 'line',
            fieldId: 'custcol_bwp_exclude_factor',
            value: 'F',
        });
        let excludeFactorBodyField = currentRec.getField({
            fieldId: 'custbody_bwp_exclude_factor',
        });
        if (excludeFactorBodyField) {
            if (excludeChecked >= 0 && excludeUnchecked >= 0) {
                // If item sublist contains both checked an unchecked lines for excludefromfactor field, 
                // then disabled the excludefromfactor field from header 
                excludeFactorBodyField.isDisabled = true;
            }
        }
    };
    exports.pageInit = pageInit;
    let fieldChanged = (scriptContext) => {
        let fieldId = scriptContext.fieldId;
        if (fieldId == 'custcol_bwp_exclude_factor') {
            // alert(`fieldId ${fieldId}`);
            let currentRec = scriptContext.currentRecord;
            let excludeFactorField = currentRec.getField({
                fieldId: 'custbody_bwp_exclude_factor',
            });
            if (excludeFactorField) {
                let isLineChange = true;
                if (!excludeFactorField.isDisabled) {
                    // Check if the change of the line field is due to a change of body field
                    let excludeFactorBody = currentRec.getValue({
                        fieldId: 'custbody_bwp_exclude_factor',
                    });
                    let excludeFactorLine = currentRec.getCurrentSublistValue({
                        sublistId: 'line',
                        fieldId: 'custcol_bwp_exclude_factor',
                    });
                    // alert(`excludeFactorBody ${excludeFactorBody} - excludeFactorLine ${excludeFactorLine}`);
                    if (excludeFactorBody == excludeFactorLine) {
                        isLineChange = false;
                    }
                }
                if (isLineChange) {
                    excludeFactorField.isDisabled = true;
                    currentRec.setValue({
                        fieldId: 'custbody_bwp_exclude_factor',
                        value: false,
                    });
                }
            }
            // let isHeaderFieldDisabled = 
        }
        if (fieldId == 'custbody_bwp_exclude_factor') {
            // alert(`fieldId ${fieldId}`);
            let currentRec = scriptContext.currentRecord;
            let excludeFactorField = currentRec.getField({
                fieldId: 'custbody_bwp_exclude_factor',
            });
            if (!excludeFactorField.isDisabled) {
                // The field value was changed manually --> apply to all lines
                alert(`Change lines`);
                let excludeFactorBody = currentRec.getValue({
                    fieldId: 'custbody_bwp_exclude_factor',
                });
                let lineCount = currentRec.getLineCount({
                    sublistId: 'line',
                });
                for (let i = 0; i < lineCount; i++) {
                    currentRec.selectLine({
                        sublistId: 'line',
                        line: i,
                    });
                    currentRec.setCurrentSublistValue({
                        sublistId: 'line',
                        fieldId: 'custcol_bwp_exclude_factor',
                        value: excludeFactorBody,
                    });
                    currentRec.commitLine({
                        sublistId: 'line',
                    });
                }
            }
        }
    };
    exports.fieldChanged = fieldChanged;
    let saveRecord = (scriptContext) => {
        let currentRec = scriptContext.currentRecord;
        let excludeChecked = currentRec.findSublistLineWithValue({
            sublistId: 'line',
            fieldId: 'custcol_bwp_exclude_factor',
            value: 'T',
        });
        let excludeUnchecked = currentRec.findSublistLineWithValue({
            sublistId: 'line',
            fieldId: 'custcol_bwp_exclude_factor',
            value: 'F',
        });
        let excludeFactorBodyField = currentRec.getField({
            fieldId: 'custbody_bwp_exclude_factor',
        });
        if (excludeFactorBodyField) {
            if (excludeChecked < 0 || excludeUnchecked < 0) {
                // All lines have the same value for exclude factor --> set the same value on exclude factor body field
                currentRec.setValue({
                    fieldId: 'custbody_bwp_exclude_factor',
                    value: excludeChecked >= 0,
                });
            }
        }
        return true;
    };
    exports.saveRecord = saveRecord;
});
