var TASKMODULE, RECORDMODULE, RUNTIMEMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType workflowactionscript
 */
define(['N/task', 'N/record', 'N/runtime'], runWorkflowAction);

function runWorkflowAction(task, record, runtime) {
	TASKMODULE= task;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	
	var returnObj = {};
	returnObj.onAction = _onAction;
	return returnObj;
}

/**
 * Defines the WorkflowAction script trigger point.
 * @param {Object} scriptContext
 * @param {Record} scriptContext.newRecord - New record
 * @param {Record} scriptContext.oldRecord - Old record
 * @param {string} scriptContext.workflowId - Internal ID of workflow which triggered this action
 * @param {string} scriptContext.type - Event type
 * @param {Form} scriptContext.form - Current form that the script uses to interact with the record
 * @since 2016.1
 */
function _onAction(scriptContext) {
	// Read step to launch form parameters
	let script = RUNTIMEMODULE.getCurrentScript();
	let extractStep = script.getParameter({
		name: 'custscript_bwp_wf_extractstep'
	});
	log.debug({
		title: 'Step to process',
		details: extractStep
	});
	let newRecord = scriptContext.newRecord;
	let extractionId = newRecord.getValue({
		fieldId: 'id'
	});
	log.debug({
		title: 'Factor extraction internalId',
		details: extractionId
	});
	
	let task;
	
	switch (extractStep) {
		case 'CUSTOMERS':
			task = TASKMODULE.create({
				taskType: TASKMODULE.TaskType.MAP_REDUCE,
				scriptId: 'customscript_bwp_mr_factorextractcust',
				deploymentId: 'customdeploy_bwp_mr_factorextractcust',
				params: {custscript_bwp_mr_factorextractidcust: extractionId}
			});
			break;
		case 'TRANSACTIONS':
			task = TASKMODULE.create({
				taskType: TASKMODULE.TaskType.MAP_REDUCE,
				scriptId: 'customscript_bwp_mr_factorextractbalance',
				deploymentId: 'customdeploy_bwp_mr_factorextractbalance',
				params: {custscript_bwp_mr_factorextractidtran: extractionId}
			});
			break;
		case 'FINALIZE':
			task = TASKMODULE.create({
				taskType: TASKMODULE.TaskType.MAP_REDUCE,
				scriptId: 'customscript_bwp_mr_factorextractfinal',
				deploymentId: 'customdeploy_bwp_mr_factorextractfinal',
				params: {custscript_bwp_mr_factorextractidfinal: extractionId}
			});
			break;
	}
	
	if (task) {
    	let stepTaskId = task.submit();
	}
}
