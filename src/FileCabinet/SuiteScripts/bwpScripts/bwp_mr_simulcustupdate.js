/**
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(["require", "exports", "N/log", "N/record", "N/search"], function (require, exports, log, recordModule, searchModule) {
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.summarize = exports.map = exports.getInputData = void 0;
    let getInputData = (scriptContext) => {
        log.debug({
            title: 'getInputData started',
            details: '',
        });
        let searchObj = searchModule.create({
            type: searchModule.Type.CUSTOMER,
            filters: [
                ['isinactive', 'is', 'F'],
                'AND',
                ['custentity_bwp_factor', 'is', 'F'],
            ],
            columns: ['internalid', 'entityid']
        });
        log.debug({
            title: 'getInputData done',
            details: '',
        });
        return searchObj;
    };
    exports.getInputData = getInputData;
    let map = (scriptContext) => {
        // log.debug({
        //     title: 'map started',
        //     details: '',
        // });
        // logRecord(context, 'map context');
        let mapValue = JSON.parse(scriptContext.value);
        log.debug({
            title: 'map',
            details: `Simulate update on customer ${mapValue.values.entityid} (${mapValue.id})`
        });
        let recordObj = recordModule.load({
            type: recordModule.Type.CUSTOMER,
            id: mapValue.id,
            isDynamic: false
        });
        try {
            recordObj.save();
        }
        catch (ex) {
            throw `${mapValue.values.entityid} (${mapValue.id}) : ${ex.name} - ${ex.message}`;
        }
        // log.debug({
        //     title: 'map done',
        //     details: '',
        // });
    };
    exports.map = map;
    let summarize = (scriptContext) => {
        log.debug({
            title: 'summarize started',
            details: '',
        });
        //	log.debug('scriptContext', scriptContext);
        let processSummaryText = '';
        let errorMessage = '';
        if (processSummaryErrors(scriptContext, 0, '') > 0) {
            errorMessage = 'Errors during data import - see error file for details';
        }
        else {
        }
        log.debug({
            title: 'summarize done',
            details: '',
        });
    };
    exports.summarize = summarize;
    function processSummaryErrors(summary, extractionId, error) {
        let errorCount = 0;
        summary.mapSummary.errors.iterator().each(function (key, error, executionNo) {
            const errorObj = JSON.parse(error);
            let message;
            if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
                message = errorObj.message;
            }
            else {
                message = error;
            }
            log.error({
                title: 'mapError',
                details: message
            });
            errorCount++;
            return true;
        });
        summary.reduceSummary.errors.iterator().each(function (key, error, executionNo) {
            const errorObj = JSON.parse(error);
            let message;
            if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
                message = errorObj.message;
            }
            else {
                message = error;
            }
            log.error({
                title: 'reduceError',
                details: message
            });
            errorCount++;
            return true;
        });
        return errorCount;
    }
    function logRecord(record, logTitle) {
        let logRecord = JSON.stringify(record) || 'UNDEFINED';
        let recNum = 0;
        while (logRecord.length > 0) {
            recNum = recNum + 1;
            log.debug({
                title: logTitle + ' record - ' + recNum,
                details: logRecord
            });
            logRecord = logRecord.substring(3900);
        }
    }
    function logVar(value, title) {
        log.debug({
            title: title + ' (' + typeof value + ')',
            details: value
        });
    }
});
